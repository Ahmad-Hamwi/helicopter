#include "Util/Auxiliary.h"
#include "Util/Transform.h"
#include "Util/Time.h"
#include "Physics/Constants.h"
#include "Util/Drawing.h"
#include "Util/Coloring.h"
#include "Util/Models.h"

using namespace Util;
using namespace Physics;

#pragma once
class Rotor
{
public:	// this values are for main rotor
	double bladesNumber;	/// 2 - 7
	double bladeMass;	/// 40 - 150kg, https://qph.fs.quoracdn.net/main-qimg-5b43786a3efb1830baba73602d37490d
	double bladeLength;	/// 20 - 27 ft, 5 - 8.2 meter
	double bladeWidth;
	double distanceFromHub;
	
	double angle;
	double angularVelocity;	// 47 - 52 rad / s
	
	double angleOfAttack;
	double angleOfAttackOffset;
	double angleOfMinimumAngleOfAttackOffset;

	const Transform transform;	// with respect to the parent transform

	Rotor(double bladesNumber, double bladeMass, double bladeLength, double bladeWidth, double distanceFromHub, Transform transform);
	
	double GetLowSpeedDragTorqueCoefficient();
	double GetHighSpeedDragTorqueCoefficient();
	double GetCriticalAngularVelocity();
	double GetDragTorque();
	double GetMomentOfInertia();
	
	void AddTorque(double);
	void SetAngularVelocityToGenerateLiftTorque(double torque, double defaultAngleOfAttack);	// this work for tail rotor only

	Vector3 GetLiftForce();
	Vector3 GetLiftTorque();	// this work for main rotor only

	void Update();
	void Draw();
};

