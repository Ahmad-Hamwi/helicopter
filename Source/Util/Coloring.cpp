#include "Coloring.h"

using namespace Util;

void Coloring::Black() {
	glColor3ub(0, 0, 0);
}
void Coloring::Red() {
	glColor3ub(255, 0, 0);
}
void Coloring::Green() {
	glColor3ub(0, 255, 0);
}
void Coloring::Green2() {
	glColor3ub(0, 128, 0);
}
void Coloring::Blue() {
	glColor3ub(0, 0, 255);
}
void Coloring::Yellow() {
	glColor3ub(255, 255, 0);
}
void Coloring::Magenta() {
	glColor3ub(255, 0, 255);
}
void Coloring::Cyan() {
	glColor3ub(0, 255, 255);
}
void Coloring::White() {
	glColor3ub(255, 255, 255);
}
void Coloring::Brown() {
	glColor3ub(128, 64, 0);
}
void Coloring::Gray() {
	glColor3ub(128, 128, 128);
}
void Coloring::RandomColor() {
	glColor3ub(rand() % (1 << 8), rand() % (1 << 8), rand() % (1 << 8));
}

void Coloring::Glass() {
	glColor4d(0.5, 0.5, 0.5, 0.5);
}