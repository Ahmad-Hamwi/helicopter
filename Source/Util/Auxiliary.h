#pragma once
#include <chrono>
#include <random>
#include <bits/stdc++.h>

using namespace std;

namespace Util
{
	const double PI = acos(-1);
	const double EPS = 1e-7;
	
	bool IsZero(double);
	bool IsEqual(double, double);
	double Sign(double);
	double Squaring(double);
	double Cubing(double);
	double Quartic(double);
	double FromRadianToDegrees(double);
	double FromDegreesToRadian(double);
	double RandomInteger(int, int);
	double RandomDouble(double, double);
	double RandomAngle();
	void MoveAngleTorwardAngle(double& angle1, double angle2, double step);
}
