#include <bits/stdc++.h>
#include "Transform.h"
#include "Drawing.h"
#include "Coloring.h"

using namespace std;

namespace Util {
	class SkyBox;
}

#pragma once
class Util::SkyBox
{
private:
	Transform transform;
	vector <int> textures;

public:
	SkyBox(Transform = Transform(), vector <int> = vector <int>(6, -1));
	void Draw();
};
