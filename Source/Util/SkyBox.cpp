#include "SkyBox.h"

using namespace Util;

SkyBox::SkyBox(Transform transform, vector <int> textures) 
				: transform(transform), textures(textures) {}

void SkyBox::Draw()
{
	glPushMatrix();
		transform.ApplyTransform();
		Drawing::DrawCuboid(1, 1, 1, Coloring::White, Coloring::White, Coloring::White, true, textures);
	glPopMatrix();
}