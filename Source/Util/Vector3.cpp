#include "Vector3.h"

using namespace Util;

Vector3::Vector3(double x, double y, double z) : x(x), y(y), z(z) {}
Vector3::Vector3(const Vector3& other) : Vector3(other.x, other.y, other.z) { }

Vector3 Vector3::operator + (const Vector3& other) const
{ 
	return Vector3(x + other.x, y + other.y, z + other.z); 
}

Vector3 Vector3::operator - (const Vector3& other) const 
{ 
	return Vector3(x - other.x, y - other.y, z - other.z); 
}

Vector3 Vector3::operator * (const double& other) const
{ 
	return Vector3(x * other, y * other, z * other); 
}

Vector3 Vector3::operator / (const double& other) const
{
	assert(!IsZero(other));
	return Vector3(x / other, y / other, z / other);
}

Vector3 Util::operator * (const double& a, const Vector3& b)
{
	return b * a;
}

Vector3 Util::operator / (const double& a, const Vector3& b)
{
	assert(!IsZero(b.x));
	assert(!IsZero(b.y));
	assert(!IsZero(b.z));
	return Vector3(a / b.x, a / b.y, a / b.z);
}

Vector3 Vector3::operator += (const Vector3& other) 
{ 
	return *this = *this + other; 
}

Vector3 Vector3::operator -= (const Vector3& other) 
{ 
	return *this = *this - other; 
}

Vector3 Vector3::operator *= (const double& other) 
{ 
	return *this = *this * other; 
}

Vector3 Vector3::operator /= (const double& other)
{
	return *this = *this / other;
}

Vector3 Vector3::operator - () const
{
	return Vector3(-x, -y, -z);
}

bool Vector3::operator == (const Vector3& other) const
{
	return IsEqual(x, other.x) && IsEqual(y, other.y) && IsEqual(z, other.z);
}

double Vector3::Magnitude() const
{
	return hypot(x, hypot(y, z));
}

double Vector3::SqrMagnitude() const
{
	return Dot(*this, *this);
}

Vector3 Vector3::Normalized() const
{
	Vector3 a = *this;
	return Normalize(a);
}

void Vector3::Draw() const
{
	glVertex3d(x, y, z);
}

void Vector3::Translate() const
{
	glTranslated(x, y, z);
}

void Vector3::Scale() const
{
	glScaled(x, y, z);
}

void Vector3::Print() const
{
	printf("%f %f %f\n", x, y, z);
}

Vector3 Vector3::Normalize(Vector3& a)
{
	return a = (IsZero(a.Magnitude()) ? a : a / a.Magnitude());
}

double Vector3::Dot(const Vector3& a, const Vector3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

Vector3 Vector3::Cross(const Vector3& a, const Vector3& b)
{
	return Vector3(a.y * b.z - a.z * b.y, 
					a.z * b.x - a.x * b.z, 
					a.x * b.y - a.y * b.x);
}

Vector3 Vector3::ElementWiseMultiplication(const Vector3& a, const Vector3& b)
{
	return Vector3(a.x * b.x,
					a.y * b.y,
					a.z * b.z);
}

double Vector3::Distnace(const Vector3& a, const Vector3& b)
{
	return (a - b).Magnitude();
}

double Vector3::SqrDistnace(const Vector3& a, const Vector3& b)
{
	return (a - b).SqrMagnitude();
}

Vector3 Vector3::Lerp(const Vector3& a, const Vector3& b, const double& t)
{
	if (t < 0) return a;
	if (t > 1) return b;
	return b * t + a * (1.0 - t);
}

Vector3 Vector3::Up()
{
	return Vector3(0, 1, 0);
}

Vector3 Vector3::Right()
{
	return Vector3(1, 0, 0);
}

Vector3 Vector3::Forward()
{
	return Vector3(0, 0, -1);
}