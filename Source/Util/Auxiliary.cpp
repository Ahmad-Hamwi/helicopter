#include "Auxiliary.h"

using namespace Util;

bool Util::IsZero(double x)
{
	return abs(x) < EPS;
}

bool Util::IsEqual(double x, double y)
{
	return IsZero(x - y);
}

double Util::Sign(double x)
{
	if (IsZero(x))	return 0;
	return x > 0 ? +1 : -1;
}

double Util::Squaring(double x)
{
	return x * x;
}

double Util::Cubing(double x)
{
	return x * x * x;
}

double Util::Quartic(double x)
{
	return x * x * x * x;
}

double Util::FromRadianToDegrees(double radian)
{
	return radian * 180.0 / PI;
}

double Util::FromDegreesToRadian(double degrees)
{
	return degrees * PI / 180.0;
}

double Util::RandomInteger(int x, int y)
{
	static mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
	return uniform_int_distribution <int>(x, y) (rng);
}

double Util::RandomDouble(double x, double y)
{
	static mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
	return uniform_real_distribution <double>(x, y) (rng);
}

double Util::RandomAngle()
{
	return RandomDouble(0, 2 * PI);
}

void Util::MoveAngleTorwardAngle(double& angle1, double angle2, double step)
{
	if (abs(angle1 - angle2) + EPS < step) {
		return;
	}

	if (abs(angle2 - angle1) < PI) {
		angle1 += Sign(angle2 - angle1) * step;
	} else {
		angle1 -= Sign(angle2 - angle1) * step;
	}
}