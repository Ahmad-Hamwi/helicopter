#include <bits/stdc++.h>
#include <windows.h>	
#include <gl\gl.h>		
#include <gl\glu.h>	
#include "Quaternion.h"
#include "Vector3.h"

using namespace std;

#pragma once

namespace Util {
	class Transform
	{
	public:
		Vector3 position;
		Vector3 scale;
		Quaternion rotation;

		Transform(Vector3 position = Vector3(), Quaternion rotation = Quaternion(), Vector3 scale = Vector3(1, 1, 1));

		// Vector3 VectorAfterTranformmingWithoutTranslating(Vector3 v) const;
		Vector3 VectorAfterTranformming(Vector3 v) const;
		Vector3 VectorAfterReverseTransformming(Vector3 v) const;
		// Transform FromTransformSpaceToWorldSpace(Transform other) const;
		void ApplyTransform() const;
		void ApplyReverseTranform() const;

		void Move(Vector3 direction, double speed);
		void Rotate(Vector3 axis, double speed);
		void Rotate(Vector3 axis, double speed, Vector3 point);
		void LookAt(const Vector3& target);

		Vector3 Up() const;
		Vector3 Right() const;
		Vector3 Forward() const;
	};
}


