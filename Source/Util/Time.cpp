#include "Time.h"

using namespace Util;

double Time::timeScale;
double Time::deltaTime;
double Time::previousTime;
double Time::currentTime;

void Time::Init()
{
	timeScale = 1;
	deltaTime = 0;
	previousTime = currentTime = GetCurrentTimeInSeconds();
}

void Time::Update()
{
	previousTime = currentTime;
	currentTime = GetCurrentTimeInSeconds();
	deltaTime = currentTime - previousTime;
}

double Time::GetDeltaTime()
{
	return deltaTime * timeScale;
}

double Time::GetCurrentTimeInSeconds()
{
	return steady_clock::now().time_since_epoch().count() / 1e9;
}