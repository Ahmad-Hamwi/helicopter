#pragma once
#include <bits/stdc++.h>

using namespace std;

namespace Util
{
	class Input
	{
	private:
		static const int N = 256;
		static bool* keys;
		static bool wasPressed[N];
		static bool InRangeKey(int);

		// mouse Inputs
		static double* mouseX;
		static double* mouseY;
		static double lastMouseX;
		static double lastMouseY;
		static bool* isLClicked;
		static bool* isRClicked;
		static double* mouseWheel;
		static double lastMouseWheel;

	public:
		static void Init(bool*, double*, double*, bool*, bool*, double*);
		static bool GetKey(int);
		static bool GetKeyDown(int);
		static bool GetKeyUp(int);
		static bool isMouseLeftButtonClicked();
		static bool isMouseRightButtonClicked();
		static double GetDeltaMouseX();
		static double GetDeltaMouseY();
		static double GetDeltaMouseWheel();
		static void Update();

		static class KeyCode
		{
		public:
			static const int A = 65;
			static const int B = 66;
			static const int C = 67;
			static const int D = 68;
			static const int E = 69;
			static const int F = 70;
			static const int G = 71;
			static const int H = 72;
			static const int I = 73;
			static const int J = 74;
			static const int K = 75;
			static const int L = 76;
			static const int M = 77;
			static const int N = 78;
			static const int O = 79;
			static const int P = 80;
			static const int Q = 81;
			static const int R = 82;
			static const int S = 83;
			static const int T = 84;
			static const int U = 85;
			static const int V = 86;
			static const int W = 87;
			static const int X = 88;
			static const int Y = 89;
			static const int Z = 90;

			static const int TAB = 9;
			static const int Shift = 16;
			static const int CTRL = 17;
			static const int CAPS_LOCK = 20;
			static const int NUM_LOCK = 144;
			static const int WINDOWS_KEY = 91;
			static const int ESCAPE = 7;
			static const int ENTER = 13;
			static const int SPACE = 32;
			static const int BACK_SPACE = 8;
			static const int PAGE_UP = 33;
			static const int PAGE_DOWN = 34;
			static const int HOME = 36;
			static const int END = 35;
			static const int INSERT = 45;
			static const int DELETE_ = 46;

			// when num_lock is turned off, this work for num_pad keys
			static const int LEFT_ARROW = 37;
			static const int UP_ARROW = 38;
			static const int DOWN_ARROW = 40;
			static const int RIGHT_ARROW = 39;
			static const int MID_ARROW = 12;
			static const int LEFT_UP_ARROW = 36;
			static const int LEFT_DOWN_ARROW = 35;
			static const int RIGHT_UP_ARROW = 33;
			static const int RIGHT_DOWN_ARROW = 34;

			// num_lock must be turned on
			static const int NUMPAD_0 = 96;
			static const int NUMPAD_1 = 97;
			static const int NUMPAD_2 = 98;
			static const int NUMPAD_3 = 99;
			static const int NUMPAD_4 = 100;
			static const int NUMPAD_5 = 101;
			static const int NUMPAD_6 = 102;
			static const int NUMPAD_7 = 103;
			static const int NUMPAD_8 = 104;
			static const int NUMPAD_9 = 105;
			static const int NUMPAD_PERIOD = 110;
			static const int NUMPAD_PLUS = 107;
			static const int NUMPAD_MINUS = 109;
			static const int NUMPAD_MULTIPLY = 106;
			static const int NUMPAD_DIVIDE = 111;
		};
	};
}