#include "Camera.h"

using namespace Util;

Camera::Camera(const Transform& targetTransform, Transform cameraTransform, Vector3 offset, double smoothFactor) : targetTransform(targetTransform)
{
	this->cameraTransform = cameraTransform;
	this->offset = offset;
	this->smoothFactor = smoothFactor;
	dist = 1;
}

void Camera::Update()
{
	static double lastClickTime = 0.0;
	static const double timeToLerp = 1.0;

	if (Input::isMouseLeftButtonClicked())
	{
		lastClickTime = Time::GetCurrentTimeInSeconds();
		cameraTransform.Rotate(cameraTransform.Up(), -Input::GetDeltaMouseX() * Time::GetDeltaTime() * 0.1, targetTransform.position);
		cameraTransform.Rotate(cameraTransform.Right(), -Input::GetDeltaMouseY() * Time::GetDeltaTime() * 0.1, targetTransform.position);
	}
	
	else if(Time::GetCurrentTimeInSeconds() - lastClickTime >= timeToLerp)
	{
		cameraTransform.rotation = Quaternion::Lerp(cameraTransform.rotation, targetTransform.rotation, 3 * Time::GetDeltaTime());
	}

	dist += -Input::GetDeltaMouseWheel() * Time::GetDeltaTime() * 0.1;

	Vector3 localOffset = cameraTransform.Right() * offset.x +
		cameraTransform.Up() * offset.y +
		cameraTransform.Forward() * offset.z;

	localOffset *= dist;

	cameraTransform.position = targetTransform.position + localOffset;
}

void Camera::Render()
{	
	Vector3 eyePoint = cameraTransform.position;
	Vector3 viewPoint = targetTransform.position + targetTransform.Forward();
	Vector3 upVector = cameraTransform.Up();

	gluLookAt(eyePoint.x, eyePoint.y, eyePoint.z,
		viewPoint.x, viewPoint.y, viewPoint.z,
		upVector.x, upVector.y, upVector.z);
}
