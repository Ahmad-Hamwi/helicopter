#include <bits/stdc++.h>
#include <windows.h>	
#include <gl\gl.h>		
#include <gl\glu.h>	
#include "Vector3.h"

using namespace std;

#pragma once

namespace Util {
	class Matrix4x4 {
	public:
		double elements[4][4];
		Matrix4x4();
		Matrix4x4(const double elements[16]);
		Matrix4x4(const Vector3 i, const Vector3 j, const Vector3 k);
		Matrix4x4 operator + (const Matrix4x4& other) const;
		Matrix4x4 operator - (const Matrix4x4& other) const;
		Matrix4x4 operator * (const double& other) const;
		Matrix4x4 operator / (const double& other) const;
		Matrix4x4 operator * (const Matrix4x4& other) const;
		Vector3 operator * (const Vector3& other) const;
		Matrix4x4 Transposed() const;

		void ToArray(double matrix[16]) const;

	};
}

