#include <bits\stdc++.h>
#include <windows.h>	
#include <gl\gl.h>		
#include <gl\glu.h>		
#pragma once

using namespace std;

namespace Util {
	class Coloring
	{
	public:
		static void Black();
		static void Red();
		static void Green();
		static void Green2();
		static void Blue();
		static void Yellow();
		static void Magenta();
		static void Cyan();
		static void White();
		static void Brown();
		static void Gray();
		static void RandomColor();
		static void Glass();
	};
}

