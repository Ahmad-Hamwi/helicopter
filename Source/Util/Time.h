#include <bits/stdc++.h>
#include <chrono>

using namespace std;
using namespace chrono;

namespace Util {
	class Time;
}

#pragma once
class Util::Time
{
private:
	static double deltaTime;
	static double previousTime;
	static double currentTime;

public:
	static double timeScale;
	static void Init();
	static void Update();
	static double GetDeltaTime();
	static double GetCurrentTimeInSeconds();
};

