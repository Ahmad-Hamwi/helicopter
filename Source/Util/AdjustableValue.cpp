#include "AdjustableValue.h"

using namespace Util;

AdjustableValue::AdjustableValue(string name, double& value, const double MINIMUM, const double MAXIMUM, 
	const double STEP, const double DEFAULT, const bool RETURN_DEFAULT_AFTER_RELEASE):
	name(name), value(value), MINIMUM(MINIMUM), MAXIMUM(MAXIMUM), STEP(STEP), 
	DEFAULT(DEFAULT), RETURN_DEFAULT_AFTER_RELEASE(RETURN_DEFAULT_AFTER_RELEASE) 
{
	assert(STEP > 0);
	assert(MINIMUM <= DEFAULT && DEFAULT <= MAXIMUM);
}

bool AdjustableValue::Increase()
{
	bool canIncrease = (value + STEP <= MAXIMUM);
	if (canIncrease) {
		value += STEP;
	}
	return canIncrease;
}

bool AdjustableValue::Decrease()
{
	bool canDecrease = (MINIMUM <= value - STEP);
	if (canDecrease) {
		value -= STEP;
	}
	return canDecrease;
}

void AdjustableValue::ReSet()
{
	if (RETURN_DEFAULT_AFTER_RELEASE) {
		value -= Sign(value - DEFAULT) * STEP;
	}
}