#include "Textures.h"

using namespace Util;

int Textures::morningBoxFront;
int Textures::morningBoxBack;
int Textures::morningBoxTop;
int Textures::morningBoxLeft;
int Textures::morningBoxRight;
int Textures::morningBoxBottom;

int Textures::nightBoxFront;
int Textures::nightBoxBack;
int Textures::nightBoxTop;
int Textures::nightBoxLeft;
int Textures::nightBoxRight;
int Textures::nightBoxBottom;

void Textures::InitAll()
{
	//SkyBox
	Textures::morningBoxFront = LoadTexture((char*)"Textures/SkyBox/front.bmp");
	Textures::morningBoxBack = LoadTexture((char*)"Textures/SkyBox/back.bmp");
	Textures::morningBoxTop = LoadTexture((char*)"Textures/SkyBox/top.bmp");
	Textures::morningBoxLeft = LoadTexture((char*)"Textures/SkyBox/left.bmp");
	Textures::morningBoxRight = LoadTexture((char*)"Textures/SkyBox/right.bmp");
	Textures::morningBoxBottom = LoadTexture((char*)"Textures/SkyBox/bottom.bmp");

	//SkyBoxNight
	Textures::nightBoxFront = LoadTexture((char*)"Textures/SkyBoxNight/front.bmp");
	Textures::nightBoxBack = LoadTexture((char*)"Textures/SkyBoxNight/back.bmp");
	Textures::nightBoxTop = LoadTexture((char*)"Textures/SkyBoxNight/top.bmp");
	Textures::nightBoxLeft = LoadTexture((char*)"Textures/SkyBoxNight/left.bmp");
	Textures::nightBoxRight = LoadTexture((char*)"Textures/SkyBoxNight/right.bmp");
	Textures::nightBoxBottom = LoadTexture((char*)"Textures/SkyBoxNight/bottom.bmp");
}