#include <bits/stdc++.h>
#include <windows.h>	
#include <gl\gl.h>		
#include <gl\glu.h>	
#include "Transform.h"
#include "Drawing.h"
#include "Matrix4x4.h"
#include "Input.h"
#include "Time.h"

using namespace std;

#pragma once

namespace Util {
	class Camera
	{
	public:
		const Transform& targetTransform;
		Transform cameraTransform;
		double smoothFactor;
		Vector3 offset;

		double dist;

		Camera(const Transform& targetTransform, Transform cameraTransform = Transform(), Vector3 offset = Vector3(), double smoothFactor = 1);
		void Update();
		void Render();
	};
}
