#include "Transform.h"

using namespace Util;

Transform::Transform(Vector3 position, Quaternion rotation, Vector3 scale)
{
	this->position = position;
	this->rotation = rotation;
	this->scale = scale;
}

/*Vector3 Transform::VectorAfterTranformmingWithoutTranslating(Vector3 v) const
{
	v = Vector3::ElementWiseMultiplication(v, scale);
	v = rotation.VectorAfterRotating(v);
	// v = v + position;

	return v;
}*/

Vector3 Transform::VectorAfterTranformming(Vector3 v) const
{
	v = Vector3::ElementWiseMultiplication(v, scale);
	v = rotation.VectorAfterRotating(v);
	v = v + position;

	return v;
}

Vector3 Transform::VectorAfterReverseTransformming(Vector3 v) const
{
	assert(!IsZero(scale.x));
	assert(!IsZero(scale.y));
	assert(!IsZero(scale.z));

	v = v - position;
	
	Quaternion rotationInv = rotation.Inversed();
	v = rotationInv.VectorAfterRotating(v);

	Vector3 scaleInv = Vector3(1.0 / scale.x, 1.0 / scale.y, 1.0 / scale.z);
	v = Vector3::ElementWiseMultiplication(v, scaleInv);

	return v;
}

/*
Transform Transform::FromTransformSpaceToWorldSpace(Transform other) const
{
	return Transform(VectorAfterTranformming(other.position),
		other.rotation * this->rotation,
		Vector3::ElementWiseMultiplication(this->scale, other.scale));
	return other;
}
*/

void Transform::ApplyTransform() const
{
	double matrix[16];
	rotation.ExportToArray(matrix);
	glTranslated(position.x, position.y, position.z);
	glMultMatrixd(matrix);
	glScaled(scale.x, scale.y, scale.z);
}

void Transform::ApplyReverseTranform() const
{
	assert(!IsZero(scale.x));
	assert(!IsZero(scale.y));
	assert(!IsZero(scale.z));

	double matrix[16];
	rotation.Inversed().ExportToArray(matrix);

	glScaled(1 / scale.x, 1 / scale.y, 1 / scale.z);
	glMultMatrixd(matrix);
	glTranslated(-position.x, -position.y, -position.z);
}

void Transform::Move(Vector3 direction, double speed)
{
	position += direction * speed;
}

void Transform::Rotate(Vector3 axis, double speed)
{
	if (axis.x != 0 || axis.y != 0 || axis.z != 0)
	{
		Quaternion step = Quaternion::AxisRotation(speed, axis);
		rotation = step * rotation;
	}
}

void Transform::Rotate(Vector3 axis, double speed, Vector3 point)
{
	if (axis.x != 0 || axis.y != 0 || axis.z != 0)
	{
		Quaternion step = Quaternion::AxisRotation(speed, axis);
		rotation = step * rotation;
		position -= point;
		position = step.VectorAfterRotating(position);
		position += point;
	}
}

Vector3 Transform::Up() const
{
	Vector3 upVector = Vector3::Up();
	upVector = rotation.VectorAfterRotating(upVector);
	Vector3::Normalize(upVector);
	return upVector;
}

Vector3 Transform::Right() const
{
	Vector3 rightVector = Vector3::Right();
	rightVector = rotation.VectorAfterRotating(rightVector);
	Vector3::Normalize(rightVector);
	return rightVector;
}

Vector3 Transform::Forward() const
{
	Vector3 forwardVector = Vector3::Forward();
	forwardVector = rotation.VectorAfterRotating(forwardVector);
	Vector3::Normalize(forwardVector);
	return forwardVector;
}

void Transform::LookAt(const Vector3& target)
{
	Vector3 newForward = target - position;
	rotation = Quaternion::FromToRotation(Forward(), newForward, Up()) * rotation;
}
