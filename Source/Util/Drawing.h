#include <bits\stdc++.h>
#include <windows.h>		
#include <gl\gl.h>			
#include <gl\glu.h>		
#include <glut.h>
#include "Coloring.h"
#include "Auxiliary.h"
#include "Vector3.h"
#include "Quaternion.h"
#pragma once

using namespace std;

namespace Util{
	class Drawing
	{
	public:
		static void DrawSquare(double Len = 1.0, 
			void(*Color)() = Coloring::White,
			bool isFull = true,
			int TextureID = -1, double RepS = 1.0, double RepT = 1.0);

		static void DrawRectangle(double Len = 1.0, double Width = 1.0,
			void(*Color)() = Coloring::White,
			bool isFull = true,
			int TextureID = -1, double RepS = 1.0, double RepT = 1.0);

		static void DrawRhombus(double Len = 1.0, double Width = 1.0, void(*Color)() = Coloring::Black, bool isFull = true);

		static void DrawCube(double Len = 1.0,
			void(*Color1)() = Coloring::Red, void(*Color2)() = Coloring::Blue, void(*Color3)() = Coloring::Green,
			bool isFull = true,
			vector <int> TextureID = vector <int>(6, -1),
			vector <double> RepS = vector <double>(6, 1.0), vector <double> RepT = vector <double>(6, 1.0));

		static void DrawCuboid(double Len = 1.0, double Width = 1.0, double Height = 1.0,
			void(*Color1)() = Coloring::Red, void(*Color2)() = Coloring::Blue, void(*Color3)() = Coloring::Green,
			bool isFull = true,
			vector <int> TextureID = vector <int>(6, -1),
			vector <double> RepS = vector <double>(6, 1.0), vector <double> RepT = vector <double>(6, 1.0));

		static void DrawCircle(double Radius = 1.0, int Slices = 20, void(*Color)() = Coloring::White, bool isFull = true);

		static void DrawSphere(double radius = 1.0,
			void(*Color)() = Coloring::White);

		static void DrawAxis(void(*MainColor)() = Coloring::Cyan, void(*Color)() = Coloring::White);

		static void DrawArrow(Vector3 direction, Vector3 color = Vector3(), double minLenght = 0.0);

		static void DrawCurvedArrow(Vector3 direction, Vector3 color, double minLenght, double radius = 1);
	};
}
