#include "Matrix4x4.h"

using namespace Util;

Matrix4x4::Matrix4x4()
{
	memset(&this->elements, 0, sizeof(this->elements));
	this->elements[0][0] = 1;
	this->elements[1][1] = 1;
	this->elements[2][2] = 1;
	this->elements[3][3] = 1;
}

Matrix4x4::Matrix4x4(const double elements[16])
{
	int k = 0;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			this->elements[j][i] = elements[k];
			k++;
		}
	}
}

Matrix4x4::Matrix4x4(const Vector3 i, const Vector3 j, const Vector3 k)
{
	memset(&this->elements, 0, sizeof(this->elements));
	this->elements[0][0] = i.x;
	this->elements[1][0] = i.y;
	this->elements[2][0] = i.z;
	this->elements[3][0] = 0;
	
	this->elements[0][1] = j.x;
	this->elements[1][1] = j.y;
	this->elements[2][1] = j.z;
	this->elements[3][1] = 0;

	this->elements[0][2] = k.x;
	this->elements[1][2] = k.y;
	this->elements[2][2] = k.z;
	this->elements[3][2] = 0;

	this->elements[0][3] = 0;
	this->elements[1][3] = 0;
	this->elements[2][3] = 0;
	this->elements[3][3] = 1;
}

Matrix4x4 Matrix4x4::operator + (const Matrix4x4& other) const
{
	Matrix4x4 res;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			res.elements[i][j] = elements[i][j] + other.elements[i][j];
		}
	}
	return res;
}

Matrix4x4 Matrix4x4::operator - (const Matrix4x4& other) const
{
	return *this + (other * -1);
}

Matrix4x4 Matrix4x4::operator * (const double& other) const
{
	Matrix4x4 res;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			res.elements[i][j] = elements[i][j] * other;
		}
	}
	return res;
}

Matrix4x4 Matrix4x4::operator / (const double& other) const
{
	assert(!IsZero(other));
	return *this * (1.0 / other);
}

Matrix4x4 Matrix4x4::operator * (const Matrix4x4& other) const
{
	Matrix4x4 res = Matrix4x4() * 0;

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 4; k++) {
				res.elements[i][j] += elements[i][k] * other.elements[k][j];
			}
		}
	}

	return res;
}

Vector3 Matrix4x4:: operator * (const Vector3& other) const
{
	Vector3 res = Vector3();

	res.x = elements[0][0] * other.x + elements[0][1] * other.y + elements[0][2] * other.z + elements[0][3];
	res.y = elements[1][0] * other.x + elements[1][1] * other.y + elements[1][2] * other.z + elements[1][3];
	res.z = elements[2][0] * other.x + elements[2][1] * other.y + elements[2][2] * other.z + elements[2][3];

	return res;
}

Matrix4x4 Matrix4x4::Transposed() const
{
	Matrix4x4 transposedMatrix = Matrix4x4();
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			transposedMatrix.elements[i][j] = elements[j][i];
		}
	}

	return transposedMatrix;
}

void Matrix4x4::ToArray(double matrix[16]) const
{
	int k = 0;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			matrix[k] = elements[j][i];
			k++;
		}
	}
}