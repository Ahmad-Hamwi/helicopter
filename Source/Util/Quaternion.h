#include <bits/stdc++.h>
#include <windows.h>	
#include <gl\gl.h>		
#include <gl\glu.h>	
#include "Vector3.h"
#include "Matrix4x4.h"

using namespace std;

#pragma once

namespace Util {
	class Quaternion {
	public:
		double w; //amount of rotation will occur.
		Vector3 u; //represents the axis about which a rotation will occur.

		Quaternion(double w = 1.0, Vector3 u = Vector3());

		//operation overloading
		Quaternion operator + (const Quaternion& other) const;
		Quaternion operator - (const Quaternion& other) const;
		Quaternion operator * (const double& other) const;
		Quaternion operator / (const double& other) const;
		Quaternion operator * (const Quaternion& other) const;
		Quaternion operator *= (const Quaternion& other);
		Quaternion operator - () const;
		
		void Print() const;

		double Magnitude() const;
		Quaternion Normalized() const;
		Quaternion Inversed() const;
		
		Vector3 VectorAfterRotating(Vector3 v) const;
		double* ExportToArray(double matrix[16]) const;
		Matrix4x4 ExportToMatrix() const;

		static Quaternion Inverse(Quaternion& a);
		static Quaternion Normalize(Quaternion& a);

		// return a quaternion that represent the rotation aroun axis by angle 
		static Quaternion AxisRotation(double angle, Vector3 axis);

		// return a lerp quaternion between a and b
		static Quaternion Lerp(const Quaternion& a, const Quaternion& b, const double& t);

		// return a quaternion that rotate vector a to vector b
		static Quaternion FromToRotation(const Vector3& a, const Vector3& b, const Vector3& up);
	};
}

