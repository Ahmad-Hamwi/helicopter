#include "Drawing.h"

using namespace Util;

void Drawing::DrawSquare(double Len, void(*Color)(), bool isFull, int TextureID, double RepS, double RepT)
{
	Color();
	glPointSize(1.0);
	glLineWidth(1.0);

	if (TextureID != -1) {
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, TextureID);
	}

	glPushMatrix();
		glScaled(Len, Len, 1.0);
		glBegin(isFull ? GL_QUADS : GL_LINE_LOOP);
			if (TextureID != -1)	glTexCoord2d(0, 0);
			glVertex3d(-0.5, -0.5, 0.0);

			if (TextureID != -1)	glTexCoord2d(RepS, 0);
			glVertex3d(+0.5, -0.5, 0.0);
			
			if (TextureID != -1)	glTexCoord2d(RepS, RepT);
			glVertex3d(+0.5, +0.5, 0.0);

			if (TextureID != -1)	glTexCoord2d(0, RepT);
			glVertex3d(-0.5, +0.5, 0.0);
		glEnd();
	glPopMatrix();

	if (TextureID != -1)	glDisable(GL_TEXTURE_2D);
}

void Drawing::DrawRectangle(double Len, double Width, void(*Color)(), bool isFull, int TextureID, double RepS, double RepT)
{
	glPushMatrix();
		glScaled(Len, Width, 1.0);
		DrawSquare(1.0, Color, isFull, TextureID, RepS, RepT);
	glPopMatrix();
}

void Drawing::DrawRhombus(double Len, double Width, void(*Color)(), bool isFull)
{
	Color();

	glPushMatrix();
	glScaled(Width, Len, 1.0);
		glBegin(isFull ? GL_POLYGON : GL_LINE_LOOP);
			glVertex3d(-0.5, +0.0, 0.0);
			glVertex3d(+0.0, +0.5, 0.0);
			glVertex3d(+0.5, +0.0, 0.0);
			glVertex3d(+0.0, -0.5, 0.0);
		glEnd();
	glPopMatrix();
}

void Drawing::DrawCube(double Len, void(*Color1)(), void(*Color2)(), void(*Color3)(), bool isFull, 
							vector <int> TextureID, vector <double> RepS, vector <double> RepT)
{
	glPushMatrix();
		glScaled(Len, Len, Len);
		
		glPushMatrix();
			glTranslated(0.0, 0.0, +0.5);
			DrawSquare(1.0, Color1, isFull, TextureID[0], RepS[0], RepT[0]);
		glPopMatrix();

		glPushMatrix();
			glTranslated(0.0, 0.0, -0.5);
			glRotated(180.0, 0.0, 1.0, 0.0);
			DrawSquare(1.0, Color1, isFull, TextureID[1], RepS[1], RepT[1]);
		glPopMatrix();

		glPushMatrix();
			glRotated(90.0, 1.0, 0.0, 0.0);
			glTranslated(0.0, 0.0, +0.5);
			DrawSquare(1.0, Color2, isFull, TextureID[2], RepS[2], RepT[2]);
		glPopMatrix();

		glPushMatrix();
			glRotated(90.0, 1.0, 0.0, 0.0);
			glTranslated(0.0, 0.0, -0.5);
			glRotated(180.0, 0.0, 1.0, 0.0);
			DrawSquare(1.0, Color2, isFull, TextureID[3], RepS[3], RepT[3]);
		glPopMatrix();

		glPushMatrix();
			glRotated(90.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, +0.5);
			DrawSquare(1.0, Color3, isFull, TextureID[4], RepS[4], RepT[4]);
		glPopMatrix();

		glPushMatrix();
			glRotated(90.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, -0.5);
			glRotated(180.0, 0.0, 1.0, 0.0);
			DrawSquare(1.0, Color3, isFull, TextureID[5], RepS[5], RepT[5]);
		glPopMatrix();

	glPopMatrix();
}

void Drawing::DrawCuboid(double Len, double Width, double Height, void(*Color1)(), void(*Color2)(), void(*Color3)(), bool isFull, 
							vector <int> TextureID, vector <double> RepS, vector <double> RepT)
{
	glPushMatrix();
		glScaled(Len, Width, Height);
		DrawCube(1.0, Color1, Color2, Color3, isFull, TextureID, RepS,	RepT);
	glPopMatrix();
}

void Drawing::DrawCircle(double Radius, int Slices, void(*Color)(), bool isFull)
{
	Color();

	glPushMatrix();
		glScaled(Radius, Radius, Radius);
		glBegin(isFull ? GL_POLYGON : GL_LINE_LOOP);
			for (int i = 0; i < Slices; i++) {
				double angle = i * 2.0 * PI / Slices;
				glVertex3d(cos(angle), sin(angle), 0.0);
			}
		glEnd();	
	glPopMatrix();
}

void Drawing::DrawSphere(double radius, void(*Color)())
{
	Color();
	glPointSize(1.0);
	glLineWidth(1.0);

	glPushMatrix();
		glScaled(radius, radius, radius);
		GLUquadric * quad = gluNewQuadric();
		gluSphere(quad, 1.0, 25, 25);
	glPopMatrix();
}

void Drawing::DrawAxis(void(*MainColor)(), void(*Color)())
{
	const double X = 6;
	const double Y = 6;
	const double Z = 6;

	const double xx = 1.0;
	const double yy = 1.0;
	const double zz = 1.0;

	Color();
	glLineWidth(1.0);
	glBegin(GL_LINES);
		for (double x = -X; x <= X; x += xx)
			for (double y = -Y; y <= Y; y += yy)
				glVertex3d(x, y, -Z), glVertex3d(x, y, +Z);

		for (double x = -X; x <= X; x += xx)
			for (double z = -Z; z <= Z; z += zz)
				glVertex3d(x, -Y, z), glVertex3d(x, +Y, z);

		for (double y = -Y; y <= Y; y += yy)
			for (double z = -Z; z <= Z; z += zz)
				glVertex3d(-X, y, z), glVertex3d(+X, y, z);
	glEnd();

	MainColor();
	glLineWidth(5.0);
	glBegin(GL_LINES);
	glVertex3d(-X, 0.0, 0.0);
	glVertex3d(+X, 0.0, 0.0);

	glVertex3d(0.0, +Y, 0.0);
	glVertex3d(0.0, -Y, 0.0);

	glVertex3d(0.0, 0.0, +Z);
	glVertex3d(0.0, 0.0, -Z);
	glEnd();
}

void Drawing::DrawArrow(Vector3 direction, Vector3 color, double minLenght)
{
	if (direction.Magnitude() > minLenght)
	{
		glColor3ub(color.x, color.y, color.z);
		glPushMatrix();

		Vector3 newRight = direction;
		Quaternion rot = Quaternion::FromToRotation(Vector3::Right(), newRight, Vector3::Up());

		if (direction.Magnitude() > 1) Vector3::Normalize(direction);
		direction = Vector3::Right() * direction.Magnitude();

		double matrix[16];
		rot.ExportToArray(matrix);

		glMultMatrixd(matrix);

		glBegin(GL_LINES);
		glVertex3d(0, 0, 0);
		glVertex3d(direction.x, direction.y, direction.z);
		glEnd();

		glTranslated(direction.x, direction.y, direction.z);
		glRotated(90, 0, 1, 0);
		glutSolidCone(0.1, 0.3, 10, 10);

		glPopMatrix();
	}
}

void Drawing::DrawCurvedArrow(Vector3 direction, Vector3 color, double minLenght, double radius)
{
	if (direction.Magnitude() > minLenght)
	{
		glLineWidth(2.5f);
		glColor3ub(color.x, color.y, color.z);
		glPushMatrix();


		if (direction.Magnitude() > 1) Vector3::Normalize(direction);
		double length = 1 / (direction.Magnitude()) * 4;

		Vector3::Normalize(direction);

		Vector3 newForward = direction;
		Vector3 newRight;
		newRight = Vector3(newForward.z, 0, -newForward.x);
		if (IsZero(direction.x) && IsZero(direction.z)) {
			newRight.x = newForward.y;
		}

		Vector3::Normalize(newRight);
		Vector3 newUp = Vector3::Cross(newForward, newRight);

		Matrix4x4 rotationMatrix = Matrix4x4(newRight, newUp, newForward);
		double matrix[16];
		rotationMatrix.ToArray(matrix);

		glMultMatrixd(matrix);

		glScaled(0.5, 0.5, 0.5);
		glBegin(GL_LINE_STRIP);

		int slices = 20;
		for (int i = 0; i < slices; i++) {
			double angle = i * 2.0 * PI / (slices * length);
			Vector3 point = Vector3::Right() * cos(angle) + Vector3::Up() * sin(angle);
			glVertex3d(point.x * radius, point.y * radius, point.z * radius);
		}

		glEnd();

		glLineWidth(1);
		double angle = (slices - 1) * 2.0 * PI / (slices * length);
		Vector3 point = Vector3::Right() * cos(angle) + Vector3::Up() * sin(angle);

		glTranslated(point.x * radius, point.y * radius, point.z * radius);

		glRotated(-90, 0, 1, 0);
		glRotated(angle * 180.0 / PI, 0, 0, 1);
		glutSolidCone(0.2, 0.3, 10, 10);

		glPopMatrix();
	}
}