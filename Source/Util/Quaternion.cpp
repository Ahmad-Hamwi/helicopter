#include "Quaternion.h"

using namespace Util;

Quaternion::Quaternion(double w, Vector3 u) : w(w), u(u) {}

Quaternion Quaternion:: operator * (const Quaternion& other) const
{
	Quaternion temp;
	temp.u = other.u * w + u * other.w + Vector3::Cross(u, other.u);
	temp.w = w * other.w - Vector3::Dot(other.u, u);
	return temp;
}

Quaternion Quaternion:: operator *= (const Quaternion& other)
{
	return *this = *this * other;
}

double Quaternion::Magnitude() const
{
	return hypot(w, u.Magnitude());
}

Quaternion Quaternion::operator + (const Quaternion& other) const
{
	return Quaternion(w + other.w, u + other.u);
}

Quaternion Quaternion::operator - (const Quaternion& other) const
{
	return Quaternion(w - other.w, u - other.u);
}

Quaternion Quaternion::operator * (const double& other) const
{
	return Quaternion(w * other, u * other);
}

Quaternion Quaternion::operator / (const double& other) const
{
	assert(!IsZero(other));
	return *this * (1.0 / other);
}

Quaternion Quaternion::operator - () const
{
	return Quaternion(-w, -u);
}

void Quaternion::Print() const
{
	printf("%f ", w);
	u.Print();
}

Quaternion Quaternion::Normalized() const
{
	Quaternion a;
	a.w = w;
	a.u = u;
	return Normalize(a);
}

Quaternion Quaternion::Inversed() const
{
	Quaternion q;
	q.w = w;
	q.u = u;
	return Inverse(q);
}

Quaternion Quaternion::Inverse(Quaternion& a)
{
	a = a.Normalized();
	a.u = a.u * -1;

	return a;
}

Quaternion Quaternion::Normalize(Quaternion& a)
{
	return a = (IsZero(a.Magnitude()) ? a : a / a.Magnitude());
}

Vector3 Quaternion::VectorAfterRotating(Vector3 v) const
{
	Quaternion p = Quaternion(0, v);
	Quaternion q = *this;	
	Quaternion qInv = q.Inversed();

	p = q * p * qInv;

	return p.u;
}


double* Quaternion::ExportToArray(double matrix[16]) const
{
	double wx, wy, wz, xx, yy, yz, xy, xz, zz;

	xx = u.x * u.x;
	xy = u.x * u.y;
	xz = u.x * u.z;
	yy = u.y * u.y;
	zz = u.z * u.z;
	yz = u.y * u.z;

	wx = w * u.x;
	wy = w * u.y;
	wz = w * u.z;

	matrix[0] = 1.0f - 2.0f * (yy + zz);
	matrix[1] = 2.0f * (xy + wz);
	matrix[2] = 2.0f * (xz - wy);
	matrix[3] = 0.0;

	matrix[4] = 2.0f * (xy - wz);
	matrix[5] = 1.0f - 2.0f * (xx + zz);
	matrix[6] = 2.0f * (yz + wx);
	matrix[7] = 0.0;

	matrix[8] = 2.0f * (xz + wy);
	matrix[9] = 2.0f * (yz - wx);
	matrix[10] = 1.0f - 2.0f * (xx + yy);
	matrix[11] = 0.0;

	matrix[12] = 0;
	matrix[13] = 0;
	matrix[14] = 0;
	matrix[15] = 1;

	return matrix;
}

Matrix4x4 Quaternion::ExportToMatrix() const
{
	double elements[16];
	ExportToArray(elements);
	return Matrix4x4(elements);
}

Quaternion Quaternion::AxisRotation(double angle, Vector3 axis)
{
	Quaternion q;
	q.w = cos(angle / 2.0);
	q.u = axis * sin(angle / 2.0);

	return q;
}

Quaternion Quaternion::Lerp(const Quaternion& a, const Quaternion& b, const double& t)
{
	if (t < 0) return a;
	if (t > 1) return b;
	Quaternion q = b * t + a * (1.0 - t);
	return Normalize(q);
}

Quaternion Quaternion::FromToRotation(const Vector3& a, const Vector3& b, const Vector3 &up)
{
	Vector3 aNormalized = a.Normalized();
	Vector3 bNormalized = b.Normalized();
	double dot = Vector3::Dot(aNormalized, bNormalized);


	if (IsEqual(dot, -1))
	{
		return Quaternion(PI, up);
	}

	if (IsEqual(dot, 1))
	{
		return Quaternion();
	}

	double rotAngle = acos(dot);
	Vector3 rotAxis = Vector3::Cross(aNormalized, bNormalized);
	Vector3::Normalize(rotAxis);

	return Quaternion::AxisRotation(rotAngle, rotAxis);
}