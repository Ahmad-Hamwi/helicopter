
#include<bits/stdc++.h>
#include <windows.h>		// Header File For Windows
#include <cmath>
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library

#include <glut.h>
#include "Coloring.h"
#include "Drawing.h"
#include "Vector3.h"
#include "Quaternion.h"
// #include "TestCube.h"
#include "Transform.h"
#include "SkyBox.h"
#include "Camera.h"
#include "Textures.h"
#include "Time.h"
#include "Rotor.h"
#include "Input.h"
#include "Helicopter.h"
#include "Models.h"
#include "AdjustableValue.h"
#include "Landscape.h"




using namespace Util;
	// Header File For The Glaux Library not working...

HDC			mainWindowhDC = NULL;		// Private GDI Device Context
HGLRC		mainWindowhRC = NULL;		// Permanent Rendering Context
HWND		mainWindowhWnd = NULL;		// Holds Our Window Handle
HINSTANCE	mainWindowhInstance;		// Holds The Instance Of The Application
bool mainWindowFullscreenFlag;
bool mainWindowActiveFlag;


HDC			sideWindowhDC = NULL;		// Private GDI Device Context
HGLRC		sideWindowhRC = NULL;		// Permanent Rendering Context
HWND		sideWindowhWnd = NULL;		// Holds Our Window Handle
HINSTANCE	sideWindowhInstance;		// Holds The Instance Of The Application
bool sideWindowFullscreenFlag;
bool sideWindowActiveFlag;

bool	keys[256];			// Array Used For The Keyboard Routine
double mouseX = 0, mouseY = 0, lastX = 0, lastY = 0;
bool isClicked = 0, isRClicked = 0;
double mouseWheelDelta = 0;
double lastMouseWheel = 0;

LRESULT	CALLBACK MainWndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
LRESULT	CALLBACK SideWndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc

GLvoid ReSizeMainWindow(GLsizei width, GLsizei height)		// Resize And Initialize The GL Window
{
	wglMakeCurrent(mainWindowhDC, mainWindowhRC);
	if (height == 0)										// Prevent A Divide By Zero By
	{
		height = 1;										// Making Height Equal One
	}

	glViewport(0, 0, width, height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	// Calculate The Aspect Ratio Of The Window
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 1000000.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

GLvoid ReSizeSideWindow(GLsizei width, GLsizei height)		// Resize And Initialize The GL Window
{
	wglMakeCurrent(sideWindowhDC, sideWindowhRC);
	if (height == 0)										// Prevent A Divide By Zero By
	{
		height = 1;										// Making Height Equal One
	}

	glViewport(0, 0, width, height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	// Calculate The Aspect Ratio Of The Window
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

Helicopter helicopter(Transform(), 750, 2, 
	Rotor(4, 35, 7, 1.0, 1.5, Transform(Vector3(0, 2.1, 0))), 
	Rotor(2, 10, 1, 0.25, 0.1, Transform(Vector3(-0.25, 0.75, 10.75), Quaternion::AxisRotation(-PI / 2.0, Vector3(0, 0, 1)))), 
	FromDegreesToRadian(10.0), Vector3(0, 5, -25), 0.5, false);




// controllers
AdjustableValue mainRotorEngineTorque("main rotor engine torque", helicopter.mainRotorEngineTorque, -100000, +100000, 2000, 0, false);
AdjustableValue tailRotorAngularVelocity("tail rotor angular velocity", helicopter.tailRotor.angularVelocity, 0, +1000, 50, 0, false);
AdjustableValue mainRotorAngleOfAttack("main rotor angle of attack", helicopter.mainRotor.angleOfAttack, 
	FromDegreesToRadian(-15), FromDegreesToRadian(+15), FromDegreesToRadian(0.5), 0, true);
AdjustableValue tailRotorAngleOfAttack("tail rotor angle of attack", helicopter.tailRotor.angleOfAttack,
	FromDegreesToRadian(5), FromDegreesToRadian(15), FromDegreesToRadian(0.5), helicopter.defaultTailRotorAngleOfAttack, true);
AdjustableValue mainRotorAngleOfAttackOffset("main rotor angle of attack offset", helicopter.mainRotor.angleOfAttackOffset, 
	FromDegreesToRadian(0), FromDegreesToRadian(5), FromDegreesToRadian(0.5), 0, true);
AdjustableValue mainRotorAngleOfMinimumAngleOfAttackOffsetX("main rotor angle of minimum angle of attack offset X", 
	*new double(0), -1.0, +1.0, 0.02, 0, true);
AdjustableValue mainRotorAngleOfMinimumAngleOfAttackOffsetY("main rotor angle of minimum angle of attack offset Y", 
	*new double(0), -1.0, +1.0, 0.02, 0, true);





// parameters
vector <AdjustableValue> parameters = {
	{"gravitational acceleration", Constants::gravitationalAcceleration,
		0, 100, 0.1, Constants::gravitationalAcceleration, false},
	{"air density", Constants::airDensity, 0, 100, 0.1, Constants::airDensity, false},
	{"air viscosity", Constants::airViscosity, 0, 1e-4, 1e-6, Constants::airViscosity, false},

	{"helicopter mass", helicopter.mass, 0.1, 50000, 500, helicopter.mass, false},
	{"helicopter radius", helicopter.radius, 0.1, 10, 0.1, helicopter.radius, false},

	{"main rotor blades number", helicopter.mainRotor.bladesNumber, 2, 10, 1, helicopter.mainRotor.bladesNumber, false},
	{"main rotor blade mass", helicopter.mainRotor.bladeMass, 0.1, 100, 1, helicopter.mainRotor.bladeMass, false},
	{"main rotor blade length", helicopter.mainRotor.bladeLength, 0.1, 10, 0.1, helicopter.mainRotor.bladeLength, false},
	{"main rotor blade width", helicopter.mainRotor.bladeWidth, 0.01, 2, 0.02, helicopter.mainRotor.bladeWidth, false},
	{"main rotor distance from hub to blade", helicopter.mainRotor.distanceFromHub, 0, 10, 0.1, helicopter.mainRotor.distanceFromHub, false},

	{"tail rotor blades number", helicopter.tailRotor.bladesNumber, 2, 8, 1, helicopter.mainRotor.bladesNumber, false},
	{"tail rotor blade mass", helicopter.tailRotor.bladeMass, 0.1, 100, 1, helicopter.mainRotor.bladeMass, false},
	{"tail rotor blade length", helicopter.tailRotor.bladeLength, 0.1, 10, 0.1, helicopter.mainRotor.bladeLength, false},
	{"tail rotor blade width", helicopter.tailRotor.bladeWidth, 0.01, 2, 0.02, helicopter.mainRotor.bladeWidth, false},
	{"tail rotor distance from hub to blade", helicopter.tailRotor.distanceFromHub, 0, 10, 0.1, helicopter.mainRotor.distanceFromHub, false}
};


//Drawing the landscape

// Wireframe view or solid view?
static bool wireframe = false;

Landscape landscape(500, 500);
void newLandscape() {
	static double rug = ((double)rand()) / RAND_MAX;
	landscape.create(rug);
}


void UpdateAndDrawSkyBox()
{
	static bool isNight = false;
	if (Input::GetKeyDown(Input::KeyCode::M))
		isNight ^= true;

<<<<<<< HEAD
//<<<<<<< HEAD
	static SkyBox morningSkyBox(Transform(Vector3(), Quaternion(), Vector3(200, 200, 200)), 
=======
	static SkyBox morningSkyBox(Transform(Vector3(), Quaternion(), 30 * Vector3(100, 100, 100)), 
>>>>>>> origin/Help
		{ Textures::morningBoxBack, Textures::morningBoxFront, Textures::morningBoxBottom,
			Textures::morningBoxTop, Textures::morningBoxLeft, Textures::morningBoxRight });
	static SkyBox nightSkyBox(Transform(Vector3(), Quaternion(), 30 * Vector3(100, 100, 100)), 
		{ Textures::nightBoxBack, Textures::nightBoxFront, Textures::nightBoxBottom, 
			Textures::nightBoxTop, Textures::nightBoxLeft, Textures::nightBoxRight });

	(isNight ? nightSkyBox : morningSkyBox).Draw();
}

int InitMainWindow(GLvoid)										// All Setup For OpenGL Goes Here
{
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	
	Time::Init();
	Textures::InitAll();
	Models::InitAll();	// don't change order of init Models and Textures
	Input::Init(keys, &mouseX, &mouseY, &isClicked, &isRClicked, &mouseWheelDelta);
	newLandscape();


	return TRUE;										// Initialization Went OK
}

int InitSideWindow(GLvoid)
{
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations

	return TRUE;
}

void drawText(char* xi_message) {
	while (*xi_message) {
		glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, *xi_message);
		xi_message++;
	}
}

void showMessage(GLfloat xi_x, GLfloat xi_y, char* xi_message, float xi_scale, void(*Color)() = Coloring::White) {
	Color();
	glPushMatrix();
	glTranslatef(xi_x, xi_y, 0.0);
	glScalef(0.0001, 0.0001, 0.0001);
	glScalef(xi_scale, xi_scale, xi_scale);
	drawText(xi_message);
	glPopMatrix();
}



void DrawMainWindow(GLvoid)									// Here's Where We Do All The Drawing
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
	glLoadIdentity();									// Reset The Current Modelview Matrix

//<<<<<<< HEAD


	


	Rotor& mainRotor = helicopter.mainRotor;
	Rotor& tailRotor = helicopter.tailRotor;
	double& mainRotorTorque = helicopter.mainRotorEngineTorque;
	//double& tailRotorTorque = helicopter.tailRotorEngineTorque;

	static const double TIME_SCALE_STEP = 2;
	if (Input::GetKeyDown(Input::KeyCode::O)) {
		Time::timeScale *= TIME_SCALE_STEP;
	} if (Input::GetKeyDown(Input::KeyCode::L)) {
		Time::timeScale /= TIME_SCALE_STEP;
	}

	/*static const double TAIL_ROTOR_TORQUE_STEP = 200;
	static const double MAXIMUM_TAIL_ROTOR_TORQUE = 2000;
	if (Input::GetKey(Input::KeyCode::T)) {
		tailRotorTorque += TAIL_ROTOR_TORQUE_STEP;
	} else if (Input::GetKey(Input::KeyCode::G)) {
		tailRotorTorque -= TAIL_ROTOR_TORQUE_STEP;
	} /*else if (!IsZero(tailRotorTorque)) {
		tailRotorTorque -= Sign(tailRotorTorque) * TAIL_ROTOR_TORQUE_STEP;
	}

	if (tailRotorTorque < -MAXIMUM_TAIL_ROTOR_TORQUE) {
		tailRotorTorque += TAIL_ROTOR_TORQUE_STEP;
	} else if (tailRotorTorque > MAXIMUM_TAIL_ROTOR_TORQUE) {
		tailRotorTorque -= TAIL_ROTOR_TORQUE_STEP;
	}
	*/


	//**********************************************************************

	/*
		TAIL ROTOR AOA
		A and D
	*/

	static const double TAIL_ROTOR_ANGLE_OF_ATTACK_STEP = FromDegreesToRadian(0.5);
	static const double TAIL_ROTOR_MINIMUM_ANGLE_OF_ATTACK_STEP = FromDegreesToRadian(5.0);
	static const double TAIL_ROTOR_MAXIMUM_ANGLE_OF_ATTACK_STEP = FromDegreesToRadian(15.0);
	if (Input::GetKey(Input::KeyCode::A)) {
		tailRotor.angleOfAttack += TAIL_ROTOR_ANGLE_OF_ATTACK_STEP;
	} else if (Input::GetKey(Input::KeyCode::D)) {
		tailRotor.angleOfAttack -= TAIL_ROTOR_ANGLE_OF_ATTACK_STEP;
	} else if (!IsEqual(tailRotor.angleOfAttack, helicopter.defaultTailRotorAngleOfAttack)) {
		tailRotor.angleOfAttack -= Sign(tailRotor.angleOfAttack - helicopter.defaultTailRotorAngleOfAttack) * TAIL_ROTOR_ANGLE_OF_ATTACK_STEP;
	}

	if (tailRotor.angleOfAttack < TAIL_ROTOR_MINIMUM_ANGLE_OF_ATTACK_STEP) {
		tailRotor.angleOfAttack += TAIL_ROTOR_ANGLE_OF_ATTACK_STEP;
	} if (tailRotor.angleOfAttack > TAIL_ROTOR_MAXIMUM_ANGLE_OF_ATTACK_STEP) {
		tailRotor.angleOfAttack -= TAIL_ROTOR_ANGLE_OF_ATTACK_STEP;
	}

	static const double MAIN_ROTOR_TORQUE_STEP = 2000;
	static const double MAXIMUM_MAIN_ROTOR_TORQUE = 200000;
//=======
	
//>>>>>>> 358d3aa54f1e30305a3aaf0244d957f1bf8499ac
	
	if (Input::GetKey(Input::KeyCode::R)) {
		mainRotorEngineTorque.Increase();
	} else if (Input::GetKey(Input::KeyCode::F)) {
		mainRotorEngineTorque.Decrease();
	} else { 
		mainRotorEngineTorque.ReSet();
	}

	if (helicopter.mainRotor.angularVelocity < 0) {
		helicopter.mainRotor.angularVelocity = 0;
		if (helicopter.mainRotorEngineTorque < 0) {
			helicopter.mainRotorEngineTorque = 0;
		}
	}

	if (!helicopter.autoRotationTailRotor) {
		if (Input::GetKey(Input::KeyCode::T)) {
			tailRotorAngularVelocity.Increase();
		} else if (Input::GetKey(Input::KeyCode::G)) {
			tailRotorAngularVelocity.Decrease();
		} else {
			tailRotorAngularVelocity.ReSet();
		}
	}

	if (Input::GetKey(Input::KeyCode::W)) {
		mainRotorAngleOfAttack.Increase();
	} else if (Input::GetKey(Input::KeyCode::S)) {
		mainRotorAngleOfAttack.Decrease();
	} else {
		mainRotorAngleOfAttack.ReSet();
	}

	if (Input::GetKey(Input::KeyCode::A)) {
		tailRotorAngleOfAttack.Increase();
	} else if (Input::GetKey(Input::KeyCode::D)) {
		tailRotorAngleOfAttack.Decrease();
	} else { 
		tailRotorAngleOfAttack.ReSet();
	}

	if (Input::GetKey(Input::KeyCode::RIGHT_ARROW)) {
		mainRotorAngleOfMinimumAngleOfAttackOffsetX.Increase();
	} else if (Input::GetKey(Input::KeyCode::LEFT_ARROW)) {
		mainRotorAngleOfMinimumAngleOfAttackOffsetX.Decrease();
	} else {
		mainRotorAngleOfMinimumAngleOfAttackOffsetX.ReSet();
	}

	if (Input::GetKey(Input::KeyCode::UP_ARROW)) {
		mainRotorAngleOfMinimumAngleOfAttackOffsetY.Increase();
	} else if (Input::GetKey(Input::KeyCode::DOWN_ARROW)) {
		mainRotorAngleOfMinimumAngleOfAttackOffsetY.Decrease();
	} else {
		mainRotorAngleOfMinimumAngleOfAttackOffsetY.ReSet();
	} 
	
	if (Input::GetKey(Input::KeyCode::RIGHT_ARROW) || Input::GetKey(Input::KeyCode::UP_ARROW) ||
		Input::GetKey(Input::KeyCode::LEFT_ARROW) || Input::GetKey(Input::KeyCode::DOWN_ARROW)) {
		mainRotorAngleOfAttackOffset.Increase();
	} else {
		mainRotorAngleOfAttackOffset.ReSet();
	}

	{
		double x = mainRotorAngleOfMinimumAngleOfAttackOffsetX.value;
		double y = mainRotorAngleOfMinimumAngleOfAttackOffsetY.value;
		if (!IsZero(x) || !IsZero(y)) {
			helicopter.mainRotor.angleOfMinimumAngleOfAttackOffset = atan2(y, x);
		}
	}

	if (Input::GetKeyDown(Input::KeyCode::Shift)) {
		helicopter.enableGravity ^= true;
	}

	if (Input::GetKeyDown(Input::KeyCode::TAB)) {
		helicopter.autoRotationTailRotor ^= true;
	}
	
	helicopter.Update();
	helicopter.Draw();
	
	//UpdateAndDrawSkyBox();
	
	glPushMatrix();
		glTranslated(0, 0, -10);
		Models::building->Draw();
	glPopMatrix();

	// Time::Update();
	// Input::Update();

<<<<<<< HEAD

	//for drawing the land
	//turn off lighting in landscape.cpp in line 134
	/*glPushMatrix();
	glTranslatef(0, -20, 0);
	wireframe ? landscape.drawWireFrame() : landscape.draw();
	glPopMatrix();*/

=======
	Models::city->Draw();

	Time::Update();
	Input::Update();
>>>>>>> origin/Help

	//DO NOT REMOVE THIS
	SwapBuffers(mainWindowhDC);
}

void DrawSideWindow(GLvoid)									// Here's Where We Do All The Drawing
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
	glLoadIdentity();									// Reset The Current Modelview Matrix

	static char buffer[256];

	static double cameraY = 0;
	static const double CAMERA_Y_STEP = 0.005;
	if (Input::GetKey(Input::KeyCode::NUMPAD_7)) {
		cameraY += CAMERA_Y_STEP;
	} if (Input::GetKey(Input::KeyCode::NUMPAD_1)) {
		cameraY -= CAMERA_Y_STEP;
	}
	gluLookAt(0, cameraY, 1, 0, cameraY, 0, 0, 1, 0);

	glPushMatrix();
	Vector3 linearVelocity = helicopter.GetLinearVelocity();
	Vector3 angularVelocity = helicopter.GetAngularVelocity();
	Vector3 mainRotorLiftForce = helicopter.mainRotor.GetLiftForce();
	Vector3 mainRotorLiftTorque = helicopter.mainRotor.GetLiftTorque();
	Vector3 tailRotorLiftForce = helicopter.tailRotor.GetLiftForce();
	Vector3 tailRotorLiftTorque = Vector3::Cross(-Vector3::Forward() * helicopter.tailRotor.transform.position.z, helicopter.tailRotor.GetLiftForce());

	/*
	sprintf(buffer, "helicopter linear velocity: %f (%f, %f, %f)\n", linearVelocity.Magnitude(),
		linearVelocity.x, linearVelocity.y, linearVelocity.z);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);
	
	Vector3 angularVelocity = helicopter.GetAngularVelocity();
	sprintf(buffer, "helicopter angular velocity: %f (%f, %f, %f)\n", angularVelocity.Magnitude(),
		angularVelocity.x, angularVelocity.y, angularVelocity.z);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.04, 0);
	
	sprintf(buffer, "%s: %f\n", mainRotorEngineTorque.name.c_str(), mainRotorEngineTorque.value);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);
	
	sprintf(buffer, "main rotor angular velocity: %f\n", helicopter.mainRotor.angularVelocity);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "%s: %f\n", mainRotorAngleOfAttack.name.c_str(), FromRadianToDegrees(mainRotorAngleOfAttack.value));
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);
	
	sprintf(buffer, "%s: %f\n", mainRotorAngleOfAttackOffset.name.c_str(), FromRadianToDegrees(mainRotorAngleOfAttackOffset.value));
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "main rotor angle of minimum angle of attack offset: %f\n", 
		FromRadianToDegrees(helicopter.mainRotor.angleOfMinimumAngleOfAttackOffset));
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	Vector3 mainRotorLiftForce = helicopter.mainRotor.GetLiftForce();
	sprintf(buffer, "main rotor lift force: %f (%f, %f, %f)\n", mainRotorLiftForce.Magnitude(), 
		mainRotorLiftForce.x, mainRotorLiftForce.y, mainRotorLiftForce.z);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	Vector3 mainRotorLiftTorque = helicopter.mainRotor.GetLiftTorque();
	sprintf(buffer, "main rotor lift torque: %f (%f, %f, %f)\n", mainRotorLiftTorque.Magnitude(), 
		mainRotorLiftTorque.x, mainRotorLiftTorque.y, mainRotorLiftTorque.z);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.04, 0);

	sprintf(buffer, "%s: %f\n", tailRotorAngleOfAttack.name.c_str(), FromRadianToDegrees(tailRotorAngleOfAttack.value));
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "tail rotor angular velocity: %f\n", helicopter.tailRotor.angularVelocity);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	/*
	Vector3 tailRotorLiftForce = helicopter.tailRotor.GetLiftForce();
	sprintf(buffer, "tail rotor lift force: %f (%f, %f, %f)\n", tailRotorLiftForce.Magnitude(), 
		tailRotorLiftForce.x, tailRotorLiftForce.y, tailRotorLiftForce.z);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	Vector3 tailRotorLiftTorque = helicopter.tailRotor.GetLiftTorque();
	sprintf(buffer, "tail rotor lift torque: %f (%f, %f, %f)\n", tailRotorLiftTorque.Magnitude(), 
		tailRotorLiftTorque.x, tailRotorLiftTorque.y, tailRotorLiftTorque.z);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);
	*/
	glTranslated(-0.75, 0.3, 0);
	glScaled(1.5, 1.5, 1.5);

	sprintf(buffer, "Adjust the parameters:\n");
	showMessage(0.0f, 0.0f, buffer, 2);
	glTranslated(0, -0.04, 0);

	static int selectedParameter = 0;
	if (Input::GetKeyDown(Input::KeyCode::NUMPAD_2)) {
		selectedParameter = (selectedParameter + 1 < int(parameters.size()) ? selectedParameter + 1 : 0);
	} if (Input::GetKeyDown(Input::KeyCode::NUMPAD_8)) {
		selectedParameter = (selectedParameter > 0 ? selectedParameter - 1 : int(parameters.size() - 1));
	}

	if (Input::GetKey(Input::KeyCode::NUMPAD_4)) {
		parameters[selectedParameter].Decrease();
	} if (Input::GetKey(Input::KeyCode::NUMPAD_6)) {
		parameters[selectedParameter].Increase();
	}

	for (int i = 0; i < int(parameters.size()); i++) {
		sprintf(buffer, "%s: %.2f\n", parameters[i].name.c_str(), parameters[i].value);
		showMessage(0.0f, 0.0f, buffer, 1, i == selectedParameter ? Coloring::Green : Coloring::White);
		glTranslated(0, -0.025, 0);
	}

	Coloring::White();

	glBegin(GL_LINES);
	glVertex3d(0.5, -10, -5);
	glVertex3d(0.5, 10, -5);
	glEnd();

	glPopMatrix();

	glPushMatrix();
	glTranslated(1.4, 1.5, -5);

	Drawing::DrawSphere(0.1);
	glutSolidSphere(0.1, 10, 10);
	
	//glLineWidth(2.0f);

	Drawing::DrawArrow(linearVelocity, Vector3(255, 0, 0), 0.1);
	Drawing::DrawArrow(helicopter.transform.rotation.VectorAfterRotating(mainRotorLiftForce), Vector3(0, 255, 0), 0.1);
	Drawing::DrawArrow(helicopter.GetGravityForce(), Vector3(255, 255, 0), 0.1);
	// Drawing::DrawArrow(helicopter.GetDragForce(), Vector3(255, 69, 0), 0.1);
	
	glPopMatrix();

	glPushMatrix();
	glTranslated(3.5, 1.5, -5);
	Drawing::DrawSphere(0.1);
	glutSolidSphere(0.1, 10, 10);

	//glLineWidth(2.0f);

	Drawing::DrawCurvedArrow(angularVelocity, Vector3(255, 0, 0), 0.1, 1.5);
	Drawing::DrawCurvedArrow(helicopter.transform.rotation.VectorAfterRotating(mainRotorLiftTorque), Vector3(0, 255, 0), 0.1, 1.5);
	Drawing::DrawCurvedArrow(helicopter.transform.rotation.VectorAfterRotating(tailRotorLiftTorque), Vector3(0, 0, 255), 0.1, 1.5);
	// Drawing::DrawCurvedArrow(helicopter.GetDragTorque(), Vector3(255, 69, 0), 0.1, 1.5);

	glPopMatrix();

	glTranslated(0.03, 0, 0);
	glScaled(1.5, 1.5, 1.5);
	sprintf(buffer, "helicopter linear velocity: %.2f (left red)\n", linearVelocity.Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "helicopter angular velocity: %.2f (right red)\n", angularVelocity.Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.04, 0);

	sprintf(buffer, "main rotor lift force: %.2f (left green)\n", mainRotorLiftForce.Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "main rotor lift torque: %.2f (right green)\n", mainRotorLiftTorque.Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "tail rotor lift torque: %.2f (right blue)\n", tailRotorLiftTorque.Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "wight force: %.2f (left yellow)\n", helicopter.GetGravityForce().Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "drag force: %.2f\n", helicopter.GetDragForce().Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "drag torque: %.2f\n", helicopter.GetDragTorque().Magnitude());
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "main rotor angular velocity: %.2f\n", helicopter.mainRotor.angularVelocity);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "%s: %.2f\n", mainRotorAngleOfAttack.name.c_str(), FromRadianToDegrees(mainRotorAngleOfAttack.value));
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "%s: %.2f\n", tailRotorAngleOfAttack.name.c_str(), FromRadianToDegrees(tailRotorAngleOfAttack.value));
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);

	sprintf(buffer, "tail rotor angular velocity: %.2f\n", helicopter.tailRotor.angularVelocity);
	showMessage(0.0f, 0.0f, buffer, 1);
	glTranslated(0, -0.02, 0);
	glPopMatrix();

	//DO NOT REMOVE THIS
	SwapBuffers(sideWindowhDC);
}

GLvoid KillGLWindow(HDC& hDC, HGLRC& hRC, HWND& hWnd, HINSTANCE& hInstance, bool& fullscreen)								// Properly Kill The Window
{
	if (fullscreen)										// Are We In Fullscreen Mode?
	{
		ChangeDisplaySettings(NULL, 0);					// If So Switch Back To The Desktop
		ShowCursor(TRUE);								// Show Mouse Pointer
	}

	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL, NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL, "Release Of DC And RC Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL, "Release Rendering Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}
		hRC = NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd, hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL, "Release Device Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hDC = NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL, "Could Not Release hWnd.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hWnd = NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("HelicopterWindow", hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL, "Could Not Unregister Class.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;									// Set hInstance To NULL
	}

	if (!UnregisterClass("HUDWindow", hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL, "Could Not Unregister Class.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
	*	title			- Title To Appear At The Top Of The Window				*
	*	width			- Width Of The GL Window Or Fullscreen Mode				*
	*	height			- Height Of The GL Window Or Fullscreen Mode			*
	*	bits			- Number Of Bits To Use For Color (8/16/24/32)			*
	*	fullscreenflag	- Use Fullscreen Mode (TRUE) Or Windowed Mode (FALSE)	*/


BOOL CreateMainWindow(char* title, int width, int height, int bits, bool fullscreenflag, HDC& hDC, HGLRC& hRC, HWND& hWnd, HINSTANCE& hInstance, bool& fullscreen)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left = (long)0;			// Set Left Value To 0
	WindowRect.right = (long)width;		// Set Right Value To Requested Width
	WindowRect.top = (long)0;				// Set Top Value To 0
	WindowRect.bottom = (long)height;		// Set Bottom Value To Requested Height

	fullscreen = fullscreenflag;			// Set The Global Fullscreen Flag

	hInstance = GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc = (WNDPROC)MainWndProc;					// WndProc Handles Messages
	wc.cbClsExtra = 0;									// No Extra Window Data
	wc.cbWndExtra = 0;									// No Extra Window Data
	wc.hInstance = hInstance;							// Set The Instance
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground = NULL;									// No Background Required For GL
	wc.lpszMenuName = NULL;									// We Don't Want A Menu
	wc.lpszClassName = "HelicopterWindow";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL, "Failed To Register The Window Class.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;											// Return FALSE
	}

	if (fullscreen)												// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth = width;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight = height;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel = bits;					// Selected Bits Per Pixel
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL, "The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?", "NeHe GL", MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
			{
				fullscreen = FALSE;		// Windowed Mode Selected.  Fullscreen = FALSE
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL, "Program Will Now Close.", "ERROR", MB_OK | MB_ICONSTOP);
				return FALSE;									// Return FALSE
			}
		}
	}

	if (fullscreen)												// Are We Still In Fullscreen Mode?
	{
		dwExStyle = WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle = WS_POPUP;										// Windows Style
		ShowCursor(FALSE);										// Hide Mouse Pointer
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle = WS_OVERLAPPEDWINDOW;							// Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd = CreateWindowEx(dwExStyle,							// Extended Style For The Window
		"HelicopterWindow",							// Class Name
		title,								// Window Title
		dwStyle |							// Defined Window Style
		WS_CLIPSIBLINGS |					// Required Window Style
		WS_CLIPCHILDREN,					// Required Window Style
		0, 0,								// Window Position
		WindowRect.right - WindowRect.left,	// Calculate Window Width
		WindowRect.bottom - WindowRect.top,	// Calculate Window Height
		NULL,								// No Parent Window
		NULL,								// No Menu
		hInstance,							// Instance
		NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Window Creation Error.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd =				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		bits,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(hDC = GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Create A GL Device Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Find A Suitable PixelFormat.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!SetPixelFormat(hDC, PixelFormat, &pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Set The PixelFormat.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(hRC = wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Create A GL Rendering Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!wglMakeCurrent(hDC, hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Activate The GL Rendering Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	ShowWindow(hWnd, SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	ReSizeMainWindow(width, height);					// Set Up Our Perspective GL Screen

	if (!InitMainWindow())									// Initialize Our Newly Created GL Window
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Initialization Failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	return TRUE;									// Success
}

BOOL CreateSideWindow(char* title, int width, int height, int bits, bool fullscreenflag, HDC& hDC, HGLRC& hRC, HWND& hWnd, HINSTANCE& hInstance, bool& fullscreen)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	sideWindowClass;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left = (long)0;			// Set Left Value To 0
	WindowRect.right = (long)width;		// Set Right Value To Requested Width
	WindowRect.top = (long)0;				// Set Top Value To 0
	WindowRect.bottom = (long)height;		// Set Bottom Value To Requested Height

	fullscreen = fullscreenflag;			// Set The Global Fullscreen Flag

	hInstance = GetModuleHandle(NULL);				// Grab An Instance For Our Window
	sideWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	sideWindowClass.lpfnWndProc = (WNDPROC)SideWndProc;					// WndProc Handles Messages
	sideWindowClass.cbClsExtra = 0;									// No Extra Window Data
	sideWindowClass.cbWndExtra = 0;									// No Extra Window Data
	sideWindowClass.hInstance = hInstance;							// Set The Instance
	sideWindowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	sideWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	sideWindowClass.hbrBackground = NULL;									// No Background Required For GL
	sideWindowClass.lpszMenuName = NULL;								// We Don't Want A Menu
	sideWindowClass.lpszClassName = "HUDWindow";								// Set The Class Name

	if (!RegisterClass(&sideWindowClass))									// Attempt To Register The Window Class
	{
		MessageBox(NULL, "Failed To Register The Window Class.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;											// Return FALSE
	}

	if (fullscreen)												// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth = width;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight = height;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel = bits;					// Selected Bits Per Pixel
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL, "The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?", "NeHe GL", MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
			{
				fullscreen = FALSE;		// Windowed Mode Selected.  Fullscreen = FALSE
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL, "Program Will Now Close.", "ERROR", MB_OK | MB_ICONSTOP);
				return FALSE;									// Return FALSE
			}
		}
	}

	if (fullscreen)												// Are We Still In Fullscreen Mode?
	{
		dwExStyle = WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle = WS_POPUP;										// Windows Style
		ShowCursor(FALSE);										// Hide Mouse Pointer
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle = WS_OVERLAPPEDWINDOW;							// Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd = CreateWindowEx(dwExStyle,							// Extended Style For The Window
		"HUDWindow",							// Class Name
		title,								// Window Title
		dwStyle |							// Defined Window Style
		WS_CLIPSIBLINGS |					// Required Window Style
		WS_CLIPCHILDREN,					// Required Window Style
		0, 0,								// Window Position
		WindowRect.right - WindowRect.left,	// Calculate Window Width
		WindowRect.bottom - WindowRect.top,	// Calculate Window Height
		NULL,								// No Parent Window
		NULL,								// No Menu
		hInstance,							// Instance
		NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Window Creation Error.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd =				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		bits,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(hDC = GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Create A GL Device Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Find A Suitable PixelFormat.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!SetPixelFormat(hDC, PixelFormat, &pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Set The PixelFormat.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(hRC = wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Create A GL Rendering Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!wglMakeCurrent(hDC, hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Can't Activate The GL Rendering Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	ShowWindow(hWnd, SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	ReSizeSideWindow(width, height);					// Set Up Our Perspective GL Screen



	if (!InitSideWindow())									// Initialize Our Newly Created GL Window
	{
		KillGLWindow(hDC, hRC, hWnd, hInstance, fullscreen);								// Reset The Display
		MessageBox(NULL, "Initialization Failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	return TRUE;									// Success
}


LRESULT CALLBACK MainWndProc(HWND	hWnd,			// Handle For This Window
	UINT	uMsg,			// Message For This Window
	WPARAM	wParam,			// Additional Message Information
	LPARAM	lParam)			// Additional Message Information
{
	static PAINTSTRUCT ps;

	switch (uMsg)									// Check For Windows Messages
	{
	case WM_TIMER:
		wglMakeCurrent(mainWindowhDC, mainWindowhRC);
		DrawMainWindow();
		return 0;

	case WM_ACTIVATE:							// Watch For Window Activate Message
	{
		if (!HIWORD(wParam))					// Check Minimization State
		{
			mainWindowActiveFlag = TRUE;						// Program Is Active
		}
		else
		{
			mainWindowActiveFlag = FALSE;						// Program Is No Longer Active
		}

		return 0;								// Return To The Message Loop
	}

	case WM_SYSCOMMAND:							// Intercept System Commands
	{
		switch (wParam)							// Check System Calls
		{
		case SC_SCREENSAVE:					// Screensaver Trying To Start?
		case SC_MONITORPOWER:				// Monitor Trying To Enter Powersave?
			return 0;							// Prevent From Happening
		}
		break;									// Exit
	}

	case WM_CLOSE:								// Did We Receive A Close Message?
	{
		PostQuitMessage(0);						// Send A Quit Message
		return 0;								// Jump Back
	}

	case WM_KEYDOWN:							// Is A Key Being Held Down?
	{
		keys[wParam] = TRUE;					// If So, Mark It As TRUE
		return 0;								// Jump Back
	}

	case WM_KEYUP:								// Has A Key Been Released?
	{
		keys[wParam] = FALSE;					// If So, Mark It As FALSE
		return 0;								// Jump Back
	}

	case WM_MOUSEMOVE:
	{
		mouseX = (float)LOWORD(lParam);   mouseY = (float)HIWORD(lParam);
		isClicked = (LOWORD(wParam) & MK_LBUTTON) ? true : false;
		isRClicked = (LOWORD(wParam) & MK_RBUTTON) ? true : false;
		break;
	}

	case WM_LBUTTONUP:
		isClicked = false; 	 break;
	case WM_RBUTTONUP:
		isRClicked = false;   break;
	case WM_LBUTTONDOWN:
		isClicked = true; 	break;
	case WM_RBUTTONDOWN:
		isRClicked = true;	break;

	case WM_MOUSEWHEEL:
	{
		mouseWheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);
		break;
	}

	case WM_SIZE:								// Resize The OpenGL Window
	{
		ReSizeMainWindow(LOWORD(lParam), HIWORD(lParam));  // LoWord=Width, HiWord=Height
		return 0;								// Jump Back
	}
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

LRESULT CALLBACK SideWndProc(HWND	hWnd,			// Handle For This Window
	UINT	uMsg,			// Message For This Window
	WPARAM	wParam,			// Additional Message Information
	LPARAM	lParam)			// Additional Message Information
{
	static PAINTSTRUCT ps;

	switch (uMsg)									// Check For Windows Messages
	{
	case WM_TIMER:
		wglMakeCurrent(sideWindowhDC, sideWindowhRC);
		DrawSideWindow();
		return 0;

	case WM_ACTIVATE:							// Watch For Window Activate Message
	{
		if (!HIWORD(wParam))					// Check Minimization State
		{
			sideWindowActiveFlag = TRUE;						// Program Is Active
		}
		else
		{
			sideWindowActiveFlag = FALSE;						// Program Is No Longer Active
		}

		return 0;								// Return To The Message Loop
	}

	case WM_SYSCOMMAND:							// Intercept System Commands
	{
		switch (wParam)							// Check System Calls
		{
		case SC_SCREENSAVE:					// Screensaver Trying To Start?
		case SC_MONITORPOWER:				// Monitor Trying To Enter Powersave?
			return 0;							// Prevent From Happening
		}
		break;									// Exit
	}

	case WM_CLOSE:								// Did We Receive A Close Message?
	{
		PostQuitMessage(0);						// Send A Quit Message
		return 0;								// Jump Back
	}
	case WM_KEYDOWN:							// Is A Key Being Held Down?
	{
		keys[wParam] = TRUE;					// If So, Mark It As TRUE
		return 0;								// Jump Back
	}

	case WM_KEYUP:								// Has A Key Been Released?
	{
		keys[wParam] = FALSE;					// If So, Mark It As FALSE
		return 0;								// Jump Back
	}

	case WM_MOUSEMOVE:
	{
		mouseX = (float)LOWORD(lParam);   mouseY = (float)HIWORD(lParam);
		isClicked = (LOWORD(wParam) & MK_LBUTTON) ? true : false;
		isRClicked = (LOWORD(wParam) & MK_RBUTTON) ? true : false;
		break;
	}

	case WM_LBUTTONUP:
		isClicked = false; 	 break;
	case WM_RBUTTONUP:
		isRClicked = false;   break;
	case WM_LBUTTONDOWN:
		isClicked = true; 	break;
	case WM_RBUTTONDOWN:
		isRClicked = true;	break;

	case WM_MOUSEWHEEL:
	{
		mouseWheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);
		break;
	}

	case WM_SIZE:								// Resize The OpenGL Window
	{
		ReSizeSideWindow(LOWORD(lParam), HIWORD(lParam));  // LoWord=Width, HiWord=Height
		return 0;								// Jump Back
	}
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE	hInstance,			// Instance
	HINSTANCE	hPrevInstance,		// Previous Instance
	LPSTR		lpCmdLine,			// Command Line Parameters
	int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	BOOL	done = FALSE;								// Bool Variable To Exit Loop

	// Ask The User Which Screen Mode They Prefer
	//if (MessageBox(NULL,"Would You Like To Run In Fullscreen Mode?", "Start FullScreen?",MB_YESNO|MB_ICONQUESTION)==IDNO)
	{
		mainWindowFullscreenFlag = FALSE;							// Windowed Mode
	}

	// Create Our OpenGL Window
	if (!CreateMainWindow((char*)"Simulation", 640, 480, 16, mainWindowFullscreenFlag, mainWindowhDC, mainWindowhRC, mainWindowhWnd, mainWindowhInstance, mainWindowFullscreenFlag))
	{
		return 0;									// Quit If Window Was Not Created
	}

	if (!CreateSideWindow((char*)"HUD", 900, 480, 16, sideWindowFullscreenFlag, sideWindowhDC, sideWindowhRC, sideWindowhWnd, sideWindowhInstance, sideWindowFullscreenFlag))
	{
		return 0;									// Quit If Window Was Not Created
	}
	//Set drawing timer to 20 frame per second
	UINT timer = SetTimer(mainWindowhWnd, 0, 16.6, (TIMERPROC)NULL);
	UINT sideTimer = SetTimer(sideWindowhWnd, 0, 16.6, (TIMERPROC)NULL);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;

}

int main(HINSTANCE hinstance, HINSTANCE hPrevInstance, LPSTR IpCmdLine, int nCmdShow)
{
	return WinMain(hinstance, hPrevInstance, IpCmdLine, nCmdShow);
}

