#include <bits/stdc++.h>
#include <windows.h>	
#include <gl\gl.h>		
#include <gl\glu.h>	
#include "Auxiliary.h"

using namespace std;

#pragma once

namespace Util {
	class Vector3
	{
	public:
		double x, y, z;

		Vector3(double x = 0.0, double y = 0.0, double z = 0.0);
		Vector3(const Vector3&);

		Vector3 operator + (const Vector3& other) const;
		Vector3 operator - (const Vector3& other) const;
		Vector3 operator * (const double& other) const;
		Vector3 operator / (const double& other) const;
		friend Vector3 operator * (const double& a, const Vector3& b);
		friend Vector3 operator / (const double& a, const Vector3& b);
		Vector3 operator += (const Vector3& other);
		Vector3 operator -= (const Vector3& other);
		Vector3 operator *= (const double& other);
		Vector3 operator /= (const double& other);
		Vector3 operator - () const;
		bool operator == (const Vector3& other) const;

		double Magnitude() const;
		double SqrMagnitude() const;
		Vector3 Normalized() const;

		void Draw() const;
		void Translate() const;
		void Scale() const;
		void Print() const;

		static Vector3 Normalize(Vector3& a);
		static double Dot(const Vector3& a, const Vector3& b);
		static Vector3 Cross(const Vector3& a, const Vector3& b);
		static Vector3 ElementWiseMultiplication(const Vector3& a, const Vector3& b);
		static double Distnace(const Vector3& a, const Vector3& b);
		static double SqrDistnace(const Vector3& a, const Vector3& b);
		static Vector3 Lerp(const Vector3& a, const Vector3& b, const double& t);

		static Vector3 Up();
		static Vector3 Right();
		static Vector3 Forward();
	};
}
