#include "Input.h"

using namespace Util;

bool* Input::keys;
bool Input::wasPressed[N];

// mouse Inputs
double* Input::mouseX;
double* Input::mouseY;
double Input::lastMouseX;
double Input::lastMouseY;
bool* Input::isLClicked;
bool* Input::isRClicked;
double* Input::mouseWheel;
double Input::lastMouseWheel;

void Input::Init(bool* _keys, double* _mouseX, double* _mouseY, bool* _isLClicked, bool* _isRClicked, double *_mouseWheel)
{
	keys = _keys;
	memset(wasPressed, false, sizeof(wasPressed));

	mouseX = _mouseX;
	mouseY = _mouseY;
	isLClicked = _isLClicked;
	isRClicked = _isRClicked;
	mouseWheel = _mouseWheel;
	lastMouseX = lastMouseY = lastMouseWheel = 0;
}

bool Input::InRangeKey(int i)
{
	return 0 <= i && i < N;
}

bool Input::GetKey(int i)
{
	assert(InRangeKey(i));
	return keys[i];
}

bool Input::GetKeyDown(int i)
{
	assert(InRangeKey(i));
	return !wasPressed[i] && keys[i];
}

bool Input::GetKeyUp(int i)
{
	assert(InRangeKey(i));
	return wasPressed[i] && !keys[i];
}

bool Input::isMouseLeftButtonClicked()
{
	return *isLClicked;
}

bool Input::isMouseRightButtonClicked()
{
	return *isRClicked;
}

double Input::GetDeltaMouseX()
{
	return *mouseX - lastMouseX;
}

double Input::GetDeltaMouseY()
{
	return *mouseY - lastMouseY;
}

double Input::GetDeltaMouseWheel()
{
	return *mouseWheel;
}

void Input::Update()
{
	for (int i = 0; i < N; i++)
		wasPressed[i] = keys[i];

	if (lastMouseWheel == *mouseWheel)
		*mouseWheel = 0;
	lastMouseWheel = *mouseWheel;
	lastMouseX = *mouseX;
	lastMouseY = *mouseY;
}