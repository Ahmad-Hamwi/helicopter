#pragma once
#include "Auxiliary.h"
#include <bits/stdc++.h>

using namespace std;

namespace Util {
	class AdjustableValue
	{
	public:
		string name;
		double& value;
		const double MINIMUM;
		const double MAXIMUM;
		const double STEP;
		const double DEFAULT;
		const bool RETURN_DEFAULT_AFTER_RELEASE;

		AdjustableValue(string name, double& value, const double MINIMUM, const double MAXIMUM, 
			const double STEP, const double DEFAULT, const bool RETURN_DEFAULT_AFTER_RELEASE);

		bool Increase();
		bool Decrease();
		void ReSet();
	};
}