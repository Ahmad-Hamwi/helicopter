#include<bits/stdc++.h>		
#include <texture.h>

// #pragma once
namespace Util {
	class Textures
	{
	public:
		//morning SkyBox
		static int morningBoxFront;
		static int morningBoxBack;
		static int morningBoxTop;
		static int morningBoxLeft;
		static int morningBoxRight;
		static int morningBoxBottom;

		//night SkyBox
		static int nightBoxFront;
		static int nightBoxBack;
		static int nightBoxTop;
		static int nightBoxLeft;
		static int nightBoxRight;
		static int nightBoxBottom;

		static void InitAll();
	};
}