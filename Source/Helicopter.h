#include <bits/stdc++.h>
#include "Physics/RigidSphere.h"
#include "Util/Camera.h"
#include "Rotor.h"
#include "Util/Models.h"

using namespace std;
using namespace Util;
using namespace Physics;


#pragma once
class Helicopter : public RigidSphere
{
public:
	Rotor mainRotor;
	Rotor tailRotor;

	double mainRotorEngineTorque;
	bool autoRotationTailRotor;
	double defaultTailRotorAngleOfAttack;

	Camera camera;
	
	Helicopter(Transform transform, double mass, double radius, Rotor mainRotor, Rotor tailRotor, 
		double defaultTailRotorAngleOfAttack, Vector3 CameraOffset, double CameraSmoothFactor, bool enableGravity);
	
	void Update();
	void Draw();
};

