#include <bits/stdc++.h>
#include <windows.h>	
#include <gl\gl.h>		
#include <gl\glu.h>
#include "Util\Transform.h"
#include "Util\Vector3.h"
#include "Util\Quaternion.h"
#include "Util\Matrix4x4.h"
#include "Util\Time.h"
#include "Physics\Constants.h"

using namespace std;
using namespace Util;

#pragma once

namespace Physics {
	class RigidBody
	{
	public:
		Transform transform;	// position and orintation
		Vector3 p;				// linear momentum
		Vector3 l;				// angular momentum
		
		double mass;
		Matrix4x4 inertia;

		bool enableGravity;

		RigidBody(Transform transform, double mass, Matrix4x4 inertia, bool enableGravity);
		
		void AddForce(Vector3 force);
		void AddTorque(Vector3 torque);
		
		Matrix4x4 GetCurrentInertiaTensor();
		Vector3 GetLinearVelocity();
		Vector3 GetAngularVelocity();
		Quaternion GetDQuaternion();
		
		Vector3 GetDragForce();
		Vector3 GetDragTorque();
		Vector3 GetGravityForce();
		
		virtual double GetCrossArea() = 0;

		virtual double GetLowSpeedDragForceCoefficient() = 0;
		virtual double GetHighSpeedDragForceCoefficient() = 0;
		double GetCriticalLinearVelocity();

		virtual double GetLowSpeedDragTorqueCoefficient() = 0;
		virtual double GetHighSpeedDragTorqueCoefficient() = 0;
		double GetCriticalAngularVelocity();
		
		void Update();
	};
}


