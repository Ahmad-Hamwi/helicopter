#pragma once

namespace Physics {
	class Constants
	{
	public:
		static double gravitationalAcceleration;
		static double airDensity;
		static double airViscosity;
	};
}