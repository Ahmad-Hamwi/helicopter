#include "RigidSphere.h"

using namespace Physics;

RigidSphere::RigidSphere(Transform transform, bool enableGravity, double mass, double radius)
	: RigidBody(transform, mass, Matrix4x4() * (5.0 / (2.0 * radius * radius * mass)), enableGravity), radius(radius) { }

double RigidSphere::GetCrossArea()
{
	return PI * Squaring(radius);
}

double RigidSphere::GetLowSpeedDragForceCoefficient()
{
	// need to fix constant
	return 10000.0 * 6 * PI * Constants::airViscosity * radius;
}

double RigidSphere::GetHighSpeedDragForceCoefficient()
{
	return 1 * 0.5 * 0.47 * Constants::airDensity * GetCrossArea();
}

// TODO: fix this function
double RigidSphere::GetLowSpeedDragTorqueCoefficient() 
{
	return 10000000.0 * 6 * PI * Constants::airViscosity * radius;
}

// TODO: fix this function
double RigidSphere::GetHighSpeedDragTorqueCoefficient()
{
	return 1000 * 0.5 * 0.47 * Constants::airDensity * GetCrossArea();
}