#pragma once
#include "RigidBody.h"

namespace Physics {
	class RigidSphere : public RigidBody
	{
	public:
		double radius;

		RigidSphere(Transform transform, bool enableGravity, double mass, double radius);

		double GetCrossArea() override;

		double GetLowSpeedDragForceCoefficient() override;
		double GetHighSpeedDragForceCoefficient() override;

		double GetLowSpeedDragTorqueCoefficient() override;
		double GetHighSpeedDragTorqueCoefficient() override;
	};
}