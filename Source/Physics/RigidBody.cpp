#include "RigidBody.h"

using namespace Physics;

RigidBody::RigidBody(Transform transform, double mass, Matrix4x4 inertia, bool enableGravity)
	: transform(transform), mass(mass), inertia(inertia), p(Vector3()), l(Vector3()), enableGravity(enableGravity) { }

void RigidBody::AddForce(Vector3 force)
{
	p += force * Time::GetDeltaTime();
}

void RigidBody::AddTorque(Vector3 torque)
{
	l += torque * Time::GetDeltaTime();
}

Matrix4x4 RigidBody::GetCurrentInertiaTensor()
{
	Matrix4x4 r = transform.rotation.ExportToMatrix();  // rotation matrix
	Matrix4x4 rTranspose = r.Transposed();
	return r * inertia * rTranspose;
}

Vector3 RigidBody::GetLinearVelocity()
{
	return p / mass;
}

Vector3 RigidBody::GetAngularVelocity()
{
	return GetCurrentInertiaTensor() * l;
}

Quaternion RigidBody::GetDQuaternion()
{
	Quaternion angularVelocity = Quaternion(0, GetAngularVelocity()) * 0.5;
	return angularVelocity * transform.rotation;
}

void RigidBody::Update()
{
	AddForce(GetDragForce());
	AddTorque(GetDragTorque());
	AddForce(GetGravityForce());

	transform.position = transform.position + GetLinearVelocity() * Time::GetDeltaTime();
	transform.rotation = transform.rotation + GetDQuaternion() * Time::GetDeltaTime();
	Quaternion::Normalize(transform.rotation);
}

Vector3 RigidBody::GetDragForce()
{
	Vector3 linearVelocity = GetLinearVelocity();
	if (linearVelocity.Magnitude() <= GetCriticalLinearVelocity()) {
		return -linearVelocity * GetLowSpeedDragForceCoefficient();
	} else {
		return -linearVelocity.Normalized() * Squaring(linearVelocity.Magnitude()) 
			* GetHighSpeedDragForceCoefficient();
	}
}

Vector3 RigidBody::GetDragTorque()
{
	Vector3 angularVelocity = GetAngularVelocity();
	if (angularVelocity.Magnitude() <= GetCriticalAngularVelocity()) {
		return -angularVelocity * GetLowSpeedDragTorqueCoefficient();
	} else {
		return -angularVelocity.Normalized() * Squaring(angularVelocity.Magnitude())
			* GetHighSpeedDragTorqueCoefficient();
	}
}

Vector3 RigidBody::GetGravityForce()
{
	return enableGravity ? Vector3(0, -Constants::gravitationalAcceleration, 0) * mass : 0;
}

double RigidBody::GetCriticalLinearVelocity()
{
	return GetLowSpeedDragForceCoefficient() / GetHighSpeedDragForceCoefficient();
}

double RigidBody::GetCriticalAngularVelocity()
{
	return GetLowSpeedDragTorqueCoefficient() / GetHighSpeedDragTorqueCoefficient();
}