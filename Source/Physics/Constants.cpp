#include "Constants.h"

using namespace Physics;

double Constants::gravitationalAcceleration = 9.80665;
double Constants::airDensity = 1.225;	// kg / m^3
double Constants::airViscosity = 1.81e-5;