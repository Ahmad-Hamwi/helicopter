#include "Helicopter.h"

using namespace Physics;

Helicopter::Helicopter(Transform transform, double mass, double radius, Rotor mainRotor, Rotor tailRotor,
	double defaultTailRotorAngleOfAttack, Vector3 CameraOffset, double CameraSmoothFactor, bool enableGravity) :
		RigidSphere(transform, enableGravity, mass, radius), defaultTailRotorAngleOfAttack(defaultTailRotorAngleOfAttack),
		mainRotor(mainRotor), tailRotor(tailRotor), camera(this->transform, Transform(), CameraOffset, CameraSmoothFactor) 
{
	mainRotorEngineTorque = 0;
	autoRotationTailRotor = true;
	this->tailRotor.angleOfAttack = defaultTailRotorAngleOfAttack;
}

void Helicopter::Update()
{
	mainRotor.AddTorque(mainRotorEngineTorque);
	if (autoRotationTailRotor) {
		// tailRotor.SetAngularVelocityToGenerateLiftTorque(mainRotorEngineTorque / tailRotor.transform.position.Magnitude(), 
		tailRotor.SetAngularVelocityToGenerateLiftTorque(mainRotorEngineTorque / tailRotor.transform.position.z,
			defaultTailRotorAngleOfAttack);
	}

	RigidSphere::AddForce(transform.rotation.VectorAfterRotating(mainRotor.GetLiftForce()));
	RigidSphere::AddTorque(transform.rotation.VectorAfterRotating(mainRotor.GetLiftTorque()));
	RigidSphere::AddTorque(-transform.Up() * mainRotorEngineTorque);

	// RigidSphere::AddForce(transform.VectorAfterTranformming(tailRotor.GetLiftForce()));		sleep this now
	// RigidSphere::AddTorque(transform.VectorAfterTranformming(tailRotor.GetLiftTorque()));	wrong
	RigidSphere::AddTorque(transform.rotation.VectorAfterRotating(
		Vector3::Cross(-Vector3::Forward() * tailRotor.transform.position.z, tailRotor.GetLiftForce())));
		// Vector3::Cross(tailRotor.transform.position, tailRotor.GetLiftForce())));
	
	mainRotor.Update();
	tailRotor.Update();

	RigidSphere::Update();
	camera.Update();
}

void Helicopter::Draw()
{
	camera.Render();

	glPushMatrix();
		transform.ApplyTransform();
		glScaled(radius / 2.0, radius / 2.0, radius / 2.0);
		Models::helicopterBody->Draw();
		mainRotor.Draw();
		tailRotor.Draw();
	glPopMatrix();
}
