#include "Rotor.h"

Rotor::Rotor(double bladesNumber, double bladeMass, double bladeLength, double bladeWidth, double distanceFromHub, Transform transform):
	bladesNumber(bladesNumber), bladeMass(bladeMass), bladeLength(bladeLength), 
	bladeWidth(bladeWidth), distanceFromHub(distanceFromHub), transform(transform)
{
	angle = angularVelocity = 0;
	angleOfAttack = angleOfAttackOffset = angleOfMinimumAngleOfAttackOffset = 0;
}

double Rotor::GetLowSpeedDragTorqueCoefficient()
{
	// random constant
	return 100000.0 * bladesNumber * (0.1 + abs(angleOfAttack)) * Constants::airViscosity * bladeLength * bladeLength * bladeWidth;
}

double Rotor::GetHighSpeedDragTorqueCoefficient()
{
	return bladesNumber * 0.5 * (0.1 + abs(angleOfAttack) / 20.0) * Constants::airDensity
		* bladeWidth * (Cubing(distanceFromHub + bladeLength) - Cubing(distanceFromHub)) / 3.0;
}

double Rotor::GetCriticalAngularVelocity()
{
	return GetLowSpeedDragTorqueCoefficient() / GetHighSpeedDragTorqueCoefficient();
}

double Rotor::GetDragTorque()
{
	if(abs(angularVelocity) <= GetCriticalAngularVelocity()) {
		return Sign(-angularVelocity) * abs(angularVelocity) * GetLowSpeedDragTorqueCoefficient();
	} else {
		return Sign(-angularVelocity) * Squaring(angularVelocity) * GetHighSpeedDragTorqueCoefficient();
	}
}

double Rotor::GetMomentOfInertia()
{
	return bladesNumber * bladeMass * ((Cubing(distanceFromHub + bladeLength) - Cubing(distanceFromHub)) * bladeWidth / 3.0 +
		bladeLength * Cubing(bladeWidth) / 12.0);
}

void Rotor::AddTorque(double torque)
{
	double angularAcceleration = torque / GetMomentOfInertia();
	angularVelocity += angularAcceleration * Time::GetDeltaTime();
}

void Rotor::SetAngularVelocityToGenerateLiftTorque(double torque, double defaultAngleOfAttack)
{
	double q = defaultAngleOfAttack;
	double r1 = distanceFromHub;
	double r2 = distanceFromHub + bladeLength;
	double denominator = bladesNumber * 0.0375 * 0.5 / 3.0 * Constants::airDensity * (Cubing(r2) - Cubing(r1))
		* bladeWidth * 2 * q * Time::GetDeltaTime();

	if (!IsZero(denominator)) {
		angularVelocity = cbrt(torque / denominator);
	}
}

Vector3 Rotor::GetLiftForce()
{
	Vector3 force = Vector3();

	for (int i = 0; i < int(bladesNumber + EPS); i++) {
		double q = angleOfAttack;
		double t = angleOfMinimumAngleOfAttackOffset;
		double u = angleOfAttackOffset;
		double q1 = q - u;
		double q2 = q + u;
		double a1 = angle + i * 2.0 * PI / bladesNumber;
		double a2 = a1 + angularVelocity * Time::GetDeltaTime();
		double r1 = distanceFromHub;
		double r2 = distanceFromHub + bladeLength;

		force += transform.Up() * 0.0375 * 0.5 / 3.0 * Constants::airDensity * Squaring(angularVelocity) * (Cubing(r2) - Cubing(r1)) * bladeWidth
			* ((q1 + q2) * (a2 - a1) + q1 * sin(a2 - t) - q2 * sin(a2 - t) - q1 * sin(a1 - t) + q2 * sin(a1 - t));
	}

	return force;
}

Vector3 Rotor::GetLiftTorque()
{
	Vector3 torque = Vector3();

	for (int i = 0; i < int(bladesNumber + EPS); i++) {
		double q = angleOfAttack;
		double t = angleOfMinimumAngleOfAttackOffset;
		double u = angleOfAttackOffset;
		double q1 = q - u;
		double q2 = q + u;
		double a1 = angle + i * 2.0 * PI / bladesNumber;
		double a2 = a1 + angularVelocity * Time::GetDeltaTime();
		double r1 = distanceFromHub;
		double r2 = distanceFromHub + bladeLength;

		Vector3 X = transform.Right();
		Vector3 Z = -transform.Forward();

		torque += 0.0375 * 0.5 * Constants::airDensity * Squaring(angularVelocity) * bladeWidth * (Quartic(r2) - Quartic(r1)) / 4.0
			* ((q1 + q2) * (sin(a2) * Z - cos(a2) * X) + (q1 - q2) * (0.25 * (sin(2 * a2 - t) + 2 * a2 * cos(t)) * Z
				+ (0.25 * 2 * a2 * sin(t) - cos(2 * a2 - t)) * X)
				- ((q1 + q2) * (sin(a1) * Z - cos(a1) * X) + (q1 - q2) * (0.25 * (sin(2 * a1 - t) + 2 * a1 * cos(t)) * Z
					+ (0.25 * 2 * a1 * sin(t) - cos(2 * a1 - t)) * X)));
	}

	return torque;
}

void Rotor::Update()
{
	AddTorque(GetDragTorque());
	angle += angularVelocity * Time::GetDeltaTime();
}

void Rotor::Draw()
{
	glPushMatrix();
	transform.ApplyTransform();

	for (int i = 0; i < int(bladesNumber + EPS); i++)
	{
		double currentAngle = angle + i * 2.0 * PI / bladesNumber;
		double currentAngleOfAttack = angleOfAttack - angleOfAttackOffset
			+ (1 - cos(currentAngle - angleOfMinimumAngleOfAttackOffset + PI / 2.0)) * angleOfAttackOffset;
		glPushMatrix();
			glRotated(FromRadianToDegrees(currentAngle), 0, 1, 0);
			
			glPushMatrix();
				glTranslated(distanceFromHub / 2.0, 0, 0);
				glScaled(distanceFromHub, 0.1 * bladeWidth, bladeWidth);
				Models::bladeConnector->Draw();
			glPopMatrix();

			glPushMatrix();
				glTranslated(distanceFromHub + bladeLength / 2.0, 0, 0);
				glRotated(FromRadianToDegrees(currentAngleOfAttack), 1, 0, 0);
				glScaled(bladeLength, 0.1 * bladeWidth, bladeWidth);
				Models::blade->Draw();
			glPopMatrix();
		glPopMatrix();
	}

	glPopMatrix();
}